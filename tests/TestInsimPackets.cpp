// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QtTest>
#include <QByteArray>


#include "InsimPackets/InsimButton.hpp"
#include "InsimPackets/InsimMessage.hpp"
#include "InsimPackets/VariableLengthInsimPacketStorage.hpp"
#include "LfsTopVersion.hpp"


class UnitTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void VariableLengthInsimPacketTest_data ();
    void VariableLengthInsimPacketTest ();

    void InsimButtonTest_data ();
    void InsimButtonTest ();

    void InsimMessageTest_data ();
    void InsimMessageTest ();
};

constexpr size_t MTC_HEADER_SIZE = 8;
constexpr size_t MTC_MAX_SIZE = 136;
constexpr size_t MTC_MAX_TEXT_BLOCK_SIZE = MTC_MAX_SIZE - MTC_HEADER_SIZE;

constexpr size_t BTN_HEADER_SIZE = 12;
constexpr size_t BTN_MAX_SIZE = 252;
constexpr size_t BTN_MAX_TEXT_BLOCK_SIZE = BTN_MAX_SIZE - BTN_HEADER_SIZE;

constexpr char LFS_CONTINUATION_BYTE = -123;




void UnitTests::VariableLengthInsimPacketTest_data () {
    auto constexpr HEADER_SIZE = MTC_HEADER_SIZE;
    auto constexpr MAX_SIZE = MTC_MAX_SIZE;
    auto constexpr MAX_TEXT_BLOCK_SIZE = MTC_MAX_TEXT_BLOCK_SIZE;

    QTest::addColumn<QByteArray>("input_str");
    QTest::addColumn<size_t>("packet_length");
    QTest::addColumn<QByteArray>("expected_resulting_text_block");

    QByteArray full_of_a = full_of_a = QByteArray(MAX_TEXT_BLOCK_SIZE - 1, 'a');
    QByteArray full_of_a_cut = full_of_a;
    full_of_a_cut[static_cast<int>(MAX_TEXT_BLOCK_SIZE - 2)] = LFS_CONTINUATION_BYTE;


    QTest::newRow("1") << QByteArray(300, 'a')
                       << MAX_SIZE
                       << full_of_a_cut;

    QTest::newRow("2") << QByteArray(MAX_TEXT_BLOCK_SIZE, 'a')
                       << MAX_SIZE
                       << full_of_a_cut;

    QTest::newRow("3") << QByteArray(MAX_TEXT_BLOCK_SIZE - 1, 'a')
                       << MAX_SIZE
                       << full_of_a;

    QTest::newRow("4") << QByteArray("1234")
                       << HEADER_SIZE + 4 + 4
                       << QByteArray("1234");

    QTest::newRow("5") << QByteArray("12345")
                       << HEADER_SIZE + 4 + 4
                       << QByteArray("12345");

    QTest::newRow("6") << QByteArray("123")
                       << HEADER_SIZE + 4
                       << QByteArray("123");

    QTest::newRow("7") << QByteArray("1")
                       << HEADER_SIZE + 4
                       << QByteArray("1");

}

void UnitTests::VariableLengthInsimPacketTest () {
    auto constexpr HEADER_SIZE = MTC_HEADER_SIZE;
    auto constexpr MAX_SIZE = MTC_MAX_SIZE;
    auto constexpr MAX_TEXT_BLOCK_SIZE = MTC_MAX_TEXT_BLOCK_SIZE;

    QFETCH(QByteArray, input_str);
    QFETCH(size_t, packet_length);
    QFETCH(QByteArray, expected_resulting_text_block);
    size_t initial_str_length = static_cast<size_t>(input_str.size());

    VariableLengthInsimPacketStorage<IS_MTC, size_t, MAX_SIZE> msg;
    msg.setText(input_str.constData(), static_cast<size_t>(input_str.size()));

    QCOMPARE(msg.getLength(), packet_length);


    if (initial_str_length >= MAX_TEXT_BLOCK_SIZE) {
        QCOMPARE(msg.getRawPtr()[MAX_SIZE - 1], '\0');
    } else {
        QCOMPARE(msg.getRawPtr()[HEADER_SIZE + initial_str_length], '\0');
    }

    QByteArray resulting_text_block (msg.getRawPtr() + 8);

    QCOMPARE(resulting_text_block, expected_resulting_text_block);
}




void UnitTests::InsimButtonTest_data () {

}

void UnitTests::InsimButtonTest () {

}


void UnitTests::InsimMessageTest_data () {

}

void UnitTests::InsimMessageTest () {

}



QTEST_APPLESS_MAIN(UnitTests)


#include "TestInsimPackets.moc"
