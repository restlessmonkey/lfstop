// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QtTest>
#include <QByteArray>

#include "TrackVoteDispatcher.hpp"

class TestTrackVoteDispatcher : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void Test_data ();
    void Test ();
};


void TestTrackVoteDispatcher::Test_data () {
    QTest::addColumn<QVector<QByteArray>>("data");

    QVector<QByteArray> row1;
    for (int i = 0; i < 30; ++i) {
        row1.push_back(QByteArray("string ").append(QByteArray::number(i)));
    }

    QTest::newRow("0")
            << row1;
}

void TestTrackVoteDispatcher::Test () {
    QFETCH(QVector<QByteArray>, data);


    TrackVoteDispatcher dsp;
    dsp.currentMode = TrackVoteDispatcher::Mode::SITE;

    dsp.prepareData();
    dsp.currentVotes[3] = 1;
    dsp.currentVotes[5] = 1;
    dsp.concludeVote();

    QVERIFY(dsp.selectedSite == 3 || dsp.selectedSite == 5);

}


QTEST_APPLESS_MAIN(TestTrackVoteDispatcher)

#include "TestTrackVoteDispatcher.moc"
