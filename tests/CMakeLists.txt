include_directories(../lfstop)

add_executable(lfstop_tests_lapsdb TestLapsDb.cpp)
add_executable(lfstop_benchmarks_lapsdb BenchmarkLapsDb.cpp)
add_executable(lfstop_benchmarks_insimwindow LapsDbInsimWindowBenchmark.cpp) # rename
add_executable(lfstop_tests_overwritable_limited_container
    OverwritableLimitedContainerTest.cpp #rename
)
add_executable(lfstop_tests_track_vote_dispatcher TestTrackVoteDispatcher.cpp)
add_executable(lfstop_tests_insim_packets TestInsimPackets.cpp)
add_executable(lfstop_tests_stintsdb TestStintsDb.cpp)

set(LFSTOP_TESTS_LIBS_TO_LINK Qt5::Test Qt5::Core ${LFSTOP_QT_ADDITIONAL_LIBRARIES} lfstop_LibLfsTop)

target_link_libraries(lfstop_tests_lapsdb ${LFSTOP_TESTS_LIBS_TO_LINK})
target_link_libraries(lfstop_benchmarks_lapsdb ${LFSTOP_TESTS_LIBS_TO_LINK})
target_link_libraries(lfstop_tests_overwritable_limited_container ${LFSTOP_TESTS_LIBS_TO_LINK})
target_link_libraries(lfstop_benchmarks_insimwindow ${LFSTOP_TESTS_LIBS_TO_LINK})
target_link_libraries(lfstop_tests_track_vote_dispatcher ${LFSTOP_TESTS_LIBS_TO_LINK})
target_link_libraries(lfstop_tests_insim_packets ${LFSTOP_TESTS_LIBS_TO_LINK})
target_link_libraries(lfstop_tests_stintsdb ${LFSTOP_TESTS_LIBS_TO_LINK})

add_library(QtCreatorHeadersFix1 EXCLUDE_FROM_ALL
                  Utils.hpp)
target_link_libraries(QtCreatorHeadersFix1
    Qt5::Core
    Qt5::Network
    ${LFSTOP_QT_ADDITIONAL_LIBRARIES}
)
