// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <limits>

#include <QtTest>
#include <QHostAddress>
#include <QTcpServer>
#include <QObject>

#include "InsimWindowManager.hpp"
#include "SwitchableInsimWindow.hpp"
#include "DataProviders/TopDataProvider.hpp"
#include "Databases/LapsDb.hpp"
#include "Databases/UsersDb.hpp"
#include "Translation.hpp"


#include <ctime>

#include <string>
#include <iostream>

std::string get_random_username () {
    static const std::string alphabet = "qwertyuiopasdfghjklzxcvbnm"
                                        "QWERTYUIOPASDFGHJKLZXCVBNM"
                                        "1234567890 _.-";

    std::string ret_str;
    const quint8 len = std::rand() % 24 + 2;
    ret_str.reserve(len);
    for (quint8 i = 0; i < len; ++i) {
        ret_str += alphabet[(size_t) std::rand() % alphabet.size()];
    }
    return ret_str;
}

class App : public QObject {
    Q_OBJECT
public:

signals:
    void finished ();

public slots:
    void run () {
        const quint16 port = 1233;
        QTcpServer*dummy = new QTcpServer();
        dummy->listen(QHostAddress::LocalHost, port);
        //std::cout << "host bound" << std::endl;

        QThread* dummy_socket_thread = new QThread(this);
        dummy->moveToThread(dummy_socket_thread);
        dummy_socket_thread->start();


        mSock = new LfsTcpSocket(this);
        mSock->connectToHost(QHostAddress::LocalHost, port);

        connect(mSock, SIGNAL(connected()), this, SLOT(connected()));

        connect(this, SIGNAL(finished()),
                dummy_socket_thread, SLOT(quit()));
    }

    void connected () {
        Translation::Translation tr_;
        Translation::TranslationBinder tr_bind (tr_, "ENG");
        std::cout << "connected!" << std::endl;
        SwitchableInsimWindow switchable_window(mSock, 0, false, "", "", &tr_bind);
        InsimWindowManager window_manager;


        QCOMPARE(mSock->state(), QAbstractSocket::ConnectedState);

        Log l("meh");
        std::shared_ptr<LapsDb> db(std::make_shared<LapsDb>(l));
        db->loadFile("databases/WE2");

        UsersDb users_db(l);

        bool show_splits = false;
        bool query_clean = false;
        TopDataProvider data_provider (db,
                                       &users_db,
                                       &show_splits,
                                       &query_clean,
                                       &tr_bind);

        data_provider.setDbName("top");

        switchable_window.setStartFrom(0);
        switchable_window.setHighlightRow(std::numeric_limits<size_t>::max());

        LapsdbQuery query { CarT::XRR(),
                            ClassT::notFound(),
                            TyreT::airioTyres() };

        std::cout << "num: "
                  << db->getLaptimesCountByQuery("top", query, false) << std::endl;

        data_provider.setQuery(query);

        switchable_window.setHeader("headerrr");
        switchable_window.setDataProvider(&data_provider);

        qint64 min_cold= std::numeric_limits<qint64>::max();
        qint64 min = std::numeric_limits<qint64>::max();
        qint64 max_cold = std::numeric_limits<qint64>::min();
        qint64 max= std::numeric_limits<qint64>::min();

        qint64 prev = 1;
        double total = 0;

        bool heated_up = false;
        constexpr int num = 10000;

        int num_cold = 0;
        int num_hot = 0;
        for (int i = 0; i < num; ++i) {
            QElapsedTimer timer;
            timer.start();
            window_manager.openWindow(&switchable_window);
            const qint64 millisecs = timer.nsecsElapsed();

            if (!heated_up) {
                const float ratio = (float) millisecs / (float) prev;
                if (ratio > 1.01f || ratio < 0.99f) {
                    prev = millisecs;
                } else {
                    heated_up = true;
                }

                if (millisecs < min_cold) {
                    min_cold = millisecs;
                }
                if (millisecs > max_cold) {
                    max_cold = millisecs;
                }
                ++num_cold;
            } else {
                if (millisecs < min) {
                    min = millisecs;
                }
                if (millisecs > max) {
                    max = millisecs;
                }

                total += millisecs;
                ++num_hot;
            }
        }

        std::cout << "min cold: " << min_cold << "\n";
        std::cout << "max cold: " << max_cold << "\n";
        std::cout << "min: " << min << "\n";
        std::cout << "max: " << max << "\n";
        std::cout << "avg: " << std::fixed << total / num_hot << "\n";



        emit finished();
    }

private:
    LfsTcpSocket *mSock;
    QEventLoop mEventLoop;
};



int main () {
    int n = 0;
    QCoreApplication app(n, 0);

    App mApp;

    QTimer::singleShot(0, &mApp, SLOT(run()));
    QObject::connect(&mApp, SIGNAL(finished()),
                     &app, SLOT(quit()));
    app.exec();
}


#include "LapsDbInsimWindowBenchmark.moc"
