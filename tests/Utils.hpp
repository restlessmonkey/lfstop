// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <string>
#include <random>


inline std::string get_random_username (std::mt19937 &pRandEngine) {
    static const std::string alphabet = "qwertyuiopasdfghjklzxcvbnm"
                                        "QWERTYUIOPASDFGHJKLZXCVBNM"
                                        "1234567890 _.-";


    std::uniform_int_distribution<size_t> length_dist (3, 24);
    std::uniform_int_distribution<size_t> laptime_dist (0, alphabet.size() - 1);

    std::string ret_str;
    const uint8_t len = static_cast<uint8_t> (length_dist(pRandEngine));
    ret_str.reserve(len);
    for (uint8_t i = 0; i < len; ++i) {
        ret_str += alphabet[laptime_dist(pRandEngine)];
    }
    return ret_str;
}


struct TestLaptimeData {
    TestLaptimeData (const CarT::InnerType pCar,
                     const quint8 pTyres,
                     const LapsDbStorage::Laptime &pLaptime,
                     bool pClean = true)
        : car (pCar),
          tyres (TyreT::fromOne(pTyres)),
          lt (pLaptime),
          clean (pClean) {
    }

    TestLaptimeData () {}

    CarT car;
    TyreT tyres;
    LapsDbStorage::Laptime lt;
    bool clean;
};

Q_DECLARE_METATYPE(TestLaptimeData)
