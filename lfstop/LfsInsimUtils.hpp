// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LFS_INSIM_UTILS_HPP_INCLUDED
#define LFSTOP_LFS_INSIM_UTILS_HPP_INCLUDED


#include <string>
#include <memory>



#include <QString>
#include <QMap>
#include <QTextDecoder>
#include <QTextEncoder>
#include <QPair>

#include "Databases/LapsDbStorage.hpp"
#include "LfsInsim.hpp"
#include "Utils/MathUtils.hpp"
#include "Utils/StdVectorUtils.hpp"


namespace LfsInsimUtils {
enum LFS_ACL_CARS : unsigned int {
     XFG =          1,
     XRG =          2,
     XRT =          4,
     RB4 =          8,
     FXO =       0x10,
     LX4 =       0x20,
     LX6 =       0x40,
     MRT =       0x80,
     UF1 =      0x100,
     RAC =      0x200,
     FZ5 =      0x400,
     FOX =      0x800,
     XFR =     0x1000,
     UFR =     0x2000,
     FO8 =     0x4000,
     FXR =     0x8000,
     XRR =    0x10000,
     FZR =    0x20000,
     BF1 =    0x40000,
     FBM =    0x80000,
};


std::string getPacketName (const unsigned char *pPacketPtr,
                           const bool pAppendDescription = false);

std::string stringStrippedColors (const std::string &pString);
std::string stringNonEscapedNonColored (const std::string &pString);

QByteArray stringStrippedColors (const QByteArray &pString);
QByteArray stringNonEscapedNonColored (const QByteArray &pString);

inline char getLatestCharset (const QByteArray &pArr,
                              const int pPos) {
    int pos = pPos;
    const std::vector<char> charsets { 'L', 'G', 'C', 'J', 'E',
                                       'T', 'B', 'H', 'S', 'K' };
    while (pos > 0) {
        --pos;
        if (pArr[pos] == '^') {
            if (vector_contains(charsets, pArr[pos + 1])) {
                return pArr[pos + 1];
            }
        }
    }
    return 'L';
}


// inserts something to "format %s string" preserving LFS encoding for the part
// after inserted data, returning the new byte array
//
// Limitations: replaces only first occurence
inline QByteArray insertStr (const QByteArray &pFormatStr,
                              const QByteArray &data_to_insert) {
    QByteArray ret = pFormatStr;
    int pos = pFormatStr.indexOf("%s");
    if (pos != -1) {
        char charset = LfsInsimUtils::getLatestCharset(pFormatStr, pos);
        ret.replace(pos, 2, data_to_insert);
        ret.insert(pos + data_to_insert.size(), '^');
        ret.insert(pos + data_to_insert.size() + 1, charset);
    }
    return ret;
}


inline void insertNamedStr (QByteArray &pDest,
                            const QByteArray &format_name,
                            const QByteArray &data_to_insert) {
    int pos = pDest.indexOf(format_name);
    if (pos != -1) {
        char charset = LfsInsimUtils::getLatestCharset(pDest, pos);
        pDest.replace(pos, format_name.size(), data_to_insert);
        pDest.insert(pos + data_to_insert.size(), '^');
        pDest.insert(pos + data_to_insert.size() + 1, charset);
    }
}

inline QByteArray coloriseStr (QByteArray pStr,
                               const char pColor,
                               const char pRevertTo = '8') {
    QByteArray ret;
    ret.reserve(pStr.size() + 4);
    ret.append('^').append(pColor).append(pStr).append('^').append(pRevertTo);
    return ret;

    // another approach:
    //pStr.reserve(pStr.size() + 4);
    //pStr.prepend(pColor).prepend('^').append('^').append(pRevertTo);
    // if this one doesn't suffice, use const ref in parameter
}

inline QByteArray coloriseStrNoRevert (QByteArray pStr,
                                       const char pColor) {
    QByteArray ret;
    ret.reserve(pStr.size() + 2);
    ret.append('^').append(pColor).append(pStr);
    return ret;

    // another approach:
    //pStr.reserve(pStr.size() + 4);
    //pStr.prepend(pColor).prepend('^').append('^').append(pRevertTo);
    // if this one doesn't suffice, use const ref in parameter
}


struct FormatPair {
    QByteArray format_str;
    QByteArray message;
};

struct pass {
    template<typename ...T> pass(T...) {}
};


template<typename... ParamsT>
QByteArray format (const QByteArray &pFormatStr, ParamsT... parameters) {
    QByteArray ret = pFormatStr;

    pass{(insertNamedStr(ret, parameters.format_str, parameters.message), 1)...};
   // pass(format_impl(ret, parameters)...);

    return ret;
}



class StringDecoder {
public:
    // should be thread-safe
    QString lfsStringToUnicode (const QByteArray &s);

    QByteArray unicodeStringToLfs (const QString &s);

    QByteArray airioNicknameToLfs (const QByteArray &s);

    QByteArray encode (const QString &data, const char pLfsMagicByte) {
        QByteArray ret_str;
        ret_str.append('^');
        ret_str.append(pLfsMagicByte);
        ret_str.append(QTextCodec::codecForName(codec_names[pLfsMagicByte])->fromUnicode(data));
        return ret_str;
    }

    static const QMap<char, char> escapes;

private:
    QString decode (const char *pData,
                    const int pLen,
                    const char pLfsMagicByte);


    char guessLfsEncoder (const QChar pChar, const char prev = 'L');


    typedef std::shared_ptr<QTextDecoder> DecPtr;
    typedef std::shared_ptr<QTextCodec> CodecPtr;

    const QMap<char, DecPtr> d =
    { { 'L', DecPtr(QTextCodec::codecForName("Windows-1252")->makeDecoder()) },
      { 'G', DecPtr(QTextCodec::codecForName("Windows-1253")->makeDecoder()) },
      { 'C', DecPtr(QTextCodec::codecForName("Windows-1251")->makeDecoder()) },
      { 'J', DecPtr(QTextCodec::codecForName("Shift-JIS")->makeDecoder()) },
      { 'E', DecPtr(QTextCodec::codecForName("Windows-1250")->makeDecoder()) },
      { 'T', DecPtr(QTextCodec::codecForName("Windows-1254")->makeDecoder()) },
      { 'B', DecPtr(QTextCodec::codecForName("Windows-1257")->makeDecoder()) },
      { 'H', DecPtr(QTextCodec::codecForName("Big5")->makeDecoder()) },
      { 'S', DecPtr(QTextCodec::codecForName("GB18030")->makeDecoder()) },
      { 'K', DecPtr(QTextCodec::codecForName("EUC-KR")->makeDecoder()) } };
    static const QMap<char, QByteArray> codec_names;
};

std::vector<QPair<bool, QByteArray> > getCarCommandArguments(const QByteArray &command);

IS_HCP createHandicapsPacket (const QMap<CarT, CarDefinition> &pCars);


inline int get_closest_space_position (const QByteArray &pArr,
                                       const int pPos,
                                       const int pLimit)
    LFSTOP_FUNCTION_ATTRIBUTE_PURE;

inline int get_closest_space_position (const QByteArray &pArr,
                                       const int pPos,
                                       const int pLimit) {

    int offset = 1;
    while (pPos + offset < pArr.size() && pArr[pPos + offset] != ' ') {
        offset *= -1;
        offset += MathUtils::sign(offset) * 1;
        if (MathUtils::abs(offset) > pLimit) {
            if (pArr[pPos - 1] == '^') {
                return pPos - 1;
            }
            return pPos;
        }
    }

    // this part seems stupid, a dirty fix shit
    if (pPos + offset > pArr.size()) {
        return pArr.size();
    } else if (pPos + offset < 0) {
        return 0;
    }

    if (pArr[pPos + offset - 1] == '^') {
        --offset;
    }
    return pPos + offset;
}

} // namespace LfsInsimUtils


#endif // LFSTOP_LFS_INSIM_UTILS_HPP_INCLUDED

