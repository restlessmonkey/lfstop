// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_PLAYERS_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_PLAYERS_DATA_PROVIDER_HPP_INCLUDED


#include <unordered_map>


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "Translation.hpp"


struct ConnectionInfo;


class PlayersDataProvider final: public INonSwitchableInsimWindowDataProvider {
public:

    PlayersDataProvider (const std::unordered_map<quint8, ConnectionInfo> * const pConns)
        : mConns (pConns){
    }

    void prepareData () override;

    // IInsimWindowDataProvider interface
    size_t getNumRows () const override {
        return mData.size();
    }
    size_t getNumColumns () const override {
        return 4;
    }
    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder &pTr) const override {
        (void) pTr;
        return mData[pN];
    }
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        const unsigned short pos_w = 5;
        const unsigned short name_w = 45;
        const unsigned short country_w = 30;
        const unsigned short additional_w = 18;

        return { { InsimButton::right, pos_w },
                 { InsimButton::left, name_w },
                 { InsimButton::left, country_w },
                 { InsimButton::left, additional_w }
        };
    }

private:
    const std::unordered_map<quint8, ConnectionInfo> * const mConns;

    std::vector<std::vector<QByteArray>> mData;
};

#endif // LFSTOP_PLAYERS_DATA_PROVIDER_HPP_INCLUDED
