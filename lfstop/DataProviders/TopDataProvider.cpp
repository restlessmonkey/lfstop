// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "TopDataProvider.hpp"


#include <QByteArray>


//#include "Utils/StringUtils.hpp"
#include "Common.hpp"
#include "Databases/LapsDb.hpp"
#include "Databases/UsersDb.hpp"


using LfsInsimUtils::coloriseStr;


std::tuple<bool, bool> TopDataProvider::setQuery (const LapsdbQuery &pQuery) {
    mQuery = pQuery;
    mTyreHide = !(mQuery.tyres == TyreT::unknownTyres());
    mCarHide = (mQuery.carsClass == ClassT::notFound());
    if (decideIfNonClean(pQuery)) {
        mQuery = pQuery;
        // strange fix for older gcc
        return std::tuple<bool, bool> { true, mQueryClean };
    } else {
        return std::tuple<bool, bool> { false, false };
    }
}

size_t TopDataProvider::getNumRows() const {
    return static_cast<size_t>(mDb->getLaptimesCountByQuery(mDbName,
                                                            mQuery,
                                                            mQueryClean));
}

const std::vector<QByteArray>
TopDataProvider::getRow (const size_t pN,
                         const Translation::TranslationBinder &pTr) const {
    (void) pTr;
    const auto lt =
        mDb->getLaptimeByQuery(mDbName, static_cast<int>(pN), mQuery, mQueryClean);

    const QByteArray more = getAdditionalInfo(lt);

    QByteArray number_field =
        QByteArray("^7") + QByteArray::number(static_cast<qulonglong>(pN + 1));
    QByteArray name_field = getNameFieldColored(lt);

    QByteArray laptime_field = QByteArray("^7") + format_time(lt.laptime);

    QByteArray laptime_data ("^7");
    if (*mShowSplits) {
        laptime_data += print_sps(lt);
    } else {
        laptime_data += print_scs(lt);
    }

    if (!lt.clean) {
        number_field[1] = '8';
        laptime_field[1] = '8';
        laptime_data[1] = '8';
    }

    return { number_field, laptime_field, name_field, laptime_data, more };
}

const QByteArray TopDataProvider::getRowColumn (const size_t pRow,
                                                const size_t pColumn) const {
    if (pColumn == 3) {
        QByteArray ret ("^7");
        const auto lt =
                mDb->getLaptimeByQuery(mDbName, static_cast<int>(pRow), mQuery, mQueryClean);
        if (*mShowSplits) {
            ret += print_sps(lt);
        } else {
            ret += print_scs(lt);
        }

        if (!lt.clean) {
            ret[1] = '8';
        }

        return ret;
    } else {
        return "u wot?";
    }
}

const std::vector<InsimWindowColumnFormat> TopDataProvider::getColumnsFormat() const {
    constexpr quint8 pos_w = 6;
    constexpr quint8 lap_w = 11;
    constexpr quint8 nam_w = 45;
    constexpr quint8 scs_w = 26;
    constexpr quint8 tyr_w = 5;
    constexpr quint8 car_w = 5;

    const unsigned char col5_width =
            (mTyreHide ? 0 : tyr_w) + (mCarHide ? 0 : car_w);

    const unsigned char col4_width = scs_w + (tyr_w + car_w) - col5_width;

    // not a problem to return too big list
    return {
        { InsimButton::right, pos_w },
        { InsimButton::right, lap_w },
        { InsimButton::left, nam_w },
        { InsimButton::center, col4_width },
        { InsimButton::center, col5_width }
    };
}

const QString
TopDataProvider::getRowToExport(const size_t pN,
                                LfsInsimUtils::StringDecoder &pDecoder,
                                const Translation::TranslationBinder &pTr) const {
    (void) pTr;

    const auto lt =
        mDb->getLaptimeByQuery(mDbName, static_cast<int>(pN), mQuery, mQueryClean);

    const QByteArray additional =
            getAdditionalInfo (lt)
            + (lt.clean ? "\tclean" : "\tnot clean");

    QByteArray number_field = QByteArray::number(static_cast<qulonglong>(pN + 1));
    QString name_field = getNameFieldNonColored(lt, pDecoder);

    QByteArray laptime_field = format_time(lt.laptime);

    QByteArray laptime_data = *mShowSplits
                              ? print_sps(lt, '\t')
                              : print_scs(lt, '\t');

    return number_field
            + '\t' + laptime_field
            + '\t' + name_field
            + '\t' + laptime_data
            + '\t' + additional;
}

const QByteArray
TopDataProvider::getAdditionalInfo (const LapsDbLaptimeAnswer &pLt) const {
    QByteArray ret;

    const bool print_tyres = (pLt.tyres != TyreT::unknownTyres()
            && pLt.tyres != TyreT::wrTyres());
    if (print_tyres) {
        ret = pLt.tyres.toString();
    }

    if (pLt.car != CarT::unknownCar()) {
        if (print_tyres) {
            ret.append(' ');
        }
        ret.append(mDb->getCarName(pLt.car));
    }
    return ret;
}

const QByteArray
TopDataProvider::getNameFieldColored (const LapsDbLaptimeAnswer &pLt) const {
    if (pLt.username[0] == '/') {
        return "^7LFSW WR^8 ("
                + pLt.username.mid(1).append(')');
    }

    return mUsersDb->userGetFullName(pLt.username);
}

const QString
TopDataProvider::getNameFieldNonColored (const LapsDbLaptimeAnswer &pLt,
                                         LfsInsimUtils::StringDecoder &pDecoder) const {
    QString name_field;
    if (pLt.username[0] == '/') {
        name_field =
            "LFSW WR ("
            + pLt.username.mid(1).append(')');
    }
    else {
        const QByteArray nickname = mUsersDb->userGetNickname(pLt.username);
        if (nickname.isEmpty()) {
            name_field = pLt.username;
        } else {
            name_field = pDecoder.lfsStringToUnicode(nickname);
            name_field.append(" (");
            name_field += pLt.username;
            name_field.append(')');
        }
    }
    return name_field;
}

bool TopDataProvider::decideIfNonClean (const LapsdbQuery &pQuery) {
    if (mDb->getLaptimesCountByQuery(mDbName, pQuery, *mQueryCleanPtr) > 0) {
        mQueryClean = *mQueryCleanPtr;
        return true;
    } else if (mDb->getLaptimesCountByQuery(mDbName, pQuery, !*mQueryCleanPtr) > 0) {
        mQueryClean = !*mQueryCleanPtr;
        return true;
    }
    return false;
}
