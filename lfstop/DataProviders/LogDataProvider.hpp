// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LOG_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_LOG_DATA_PROVIDER_HPP_INCLUDED


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "Utils/MathUtils.hpp"
#include "Utils/OverwritableLimitedContainer.hpp"
#include "LfsInsimUtils.hpp"
#include "CompilerUtils.hpp"


using LfsInsimUtils::get_closest_space_position;


class LogDataProvider final: public INonSwitchableInsimWindowDataProvider {
public:
    explicit LogDataProvider (const size_t pNumLogEntries)
        : mData(pNumLogEntries) {
    }

    // INonSwitchableInsimWindowDataProvider interface
    void prepareData () override {}
    size_t getNumRows () const override {
        return static_cast<size_t>(mData.size());
    }
    size_t getNumColumns () const override {
        return 2;
    }
    const std::vector<QByteArray> getRow (const size_t pN,
                                          const Translation::TranslationBinder &pTr) const override{
        (void) pTr;
        return mData[pN];
    }
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        return { { InsimButton::left, 15 },
                 { InsimButton::left, 83 }
        };
    }


    void addMessage (const QByteArray &pMessage) {
        constexpr int max_string_length = 72;
        if (pMessage.size() <= max_string_length) {
            mData.push_back({
                QDateTime::currentDateTime().toString("dd.MM hh:mm:ss")
                                            .toLatin1(),
                pMessage });
        } else {
            const int wrap_str = get_closest_space_position(pMessage, 68, 10);
            mData.push_back({
                QDateTime::currentDateTime().toString("dd.MM hh:mm:ss")
                                            .toLatin1(),
                pMessage.left(wrap_str) });
            if (wrap_str < pMessage.size()) {
                QByteArray second_part;
                second_part.reserve(pMessage.size() - wrap_str + 2);

                const char charset = LfsInsimUtils::getLatestCharset(pMessage,
                                                                     wrap_str);
                if (charset != 'L') {
                    second_part.append('^');
                    second_part.append(charset);
                }
                second_part.append(pMessage.mid(wrap_str));

                mData.push_back({ QByteArray(),
                                  second_part });
            }
        }
    }

private:
    OverwritableLimitedContainer<std::vector<QByteArray>> mData;
};

#endif // LFSTOP_LOG_DATA_PROVIDER_HPP_INCLUDED
