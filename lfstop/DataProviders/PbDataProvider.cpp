// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "PbDataProvider.hpp"


#include "Databases/LapsDb.hpp"
#include "Utils/StringUtils.hpp"
#include "Common.hpp"


const QByteArray PbDataProvider::getRowColumn (const size_t pRow,
                                               const size_t pColumn) const {
    assert(pColumn == 1);
    (void) pColumn;
    if (pRow % 2) {
        return mData[pRow][1];
    }

    const LapsDbStorage::Laptime &pb = mLaptimesCache[pRow / 2].first;
    const TyreT actual_tyres = mLaptimesCache[pRow / 2].second;

    const QByteArray lt_data = " ^8" + (*mShowSplits
                                        ? print_sps(pb)
                                        : print_scs(pb));

    return "^7" + format_time(pb.laptime)
            + lt_data
            + " " + actual_tyres.toString();
}

void PbDataProvider::getData (const QByteArray &pDbName,
                              const QByteArray &pHeader) {
    using StringUtils::toString;

    LapsDbStorage::Laptime pb;
    TyreT tyres = mQuery.tyres;
    try {
        auto answer = mDb->findUsernameLaptime(pDbName,
                                               mUsername,
                                               mQuery.car,
                                               mQuery.tyres,
                                               true);
        pb = answer;
        tyres = answer.tyres;
    } catch (LapsDb::LapNotFoundException) {
        return;
    }

    const QByteArray lt_data = " ^8" + (*mShowSplits
                                        ? print_sps(pb)
                                        : print_scs(pb));

    mLaptimesCache.push_back(QPair<LapsDbStorage::Laptime, TyreT>{pb, tyres} );
    mData.push_back({ pHeader,
                      "^7" + format_time(pb.laptime)
                      + lt_data
                      + " " + tyres.toString() });
    mData.push_back(
    { "",
      (mQuery.tyres == TyreT::unknownTyres()
      ? QByteArray::number(mDb->findUserLaptimeNum(pDbName,
                                                   mUsername,
                                                   mQuery.car,
                                                   TyreT::unknownTyres(),
                                                   true) + 1)
        + "/" + QByteArray::number(mDb->getLaptimesCount(pDbName,
                                                         mQuery.car,
                                                         TyreT::unknownTyres()))
        + LfsInsimUtils::insertStr(" %s ",
                                   mTr->get(Translation::StringIds::FROM_ALL))
      : "")
      + QByteArray::number(mDb->findUserLaptimeNum(pDbName,
                                                   mUsername,
                                                   mQuery.car,
                                                   tyres,
                                                   true) + 1)
      + "/" + QByteArray::number(mDb->getLaptimesCount(pDbName,
                                                       mQuery.car,
                                                       tyres))
      + LfsInsimUtils::insertStr(" %s",
                                 mTr->get(Translation::StringIds::FROM_THIS_TYRES))});

}


