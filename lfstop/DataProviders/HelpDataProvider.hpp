// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_HELP_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_HELP_DATA_PROVIDER_HPP_INCLUDED


#include <utility>


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "Translation.hpp"
#include "CustomInfoDataProvider.hpp"

inline void
remove_command_from_help_data (std::vector<std::tuple<QByteArray,
                                                      bool,
                                                      Translation::StringIds::Ids>> &pList,
                               const QByteArray &pCommand) {
    pList.erase(std::remove_if(pList.begin(),
                               pList.end(),
                               [&pCommand] (const std::tuple<QByteArray,
                                                             bool,
                                                             Translation::StringIds::Ids> &el) {
                                   return std::get<0>(el) == pCommand;
                               }),
                pList.end());
}


class HelpDataProvider final: public INonSwitchableInsimWindowDataProvider {
public:
    HelpDataProvider (std::map<std::string, CustomInfoDataProvider> &pCustomInfoDataProviders)
        : mData ({
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "top [car|class] [tyres] [username|#]",
                     true,
                     Translation::StringIds::SERVER_BEST_LAPS),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "tb [car|class] [tyres] [username|#]",
                     true,
                     Translation::StringIds::BEST_POSSIBLE_LAPS_EVER),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "tbs [car|class] [tyres] [username|#]",
                     true,
                     Translation::StringIds::STINT_BEST_POSSIBLE_LAPS),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "avg [car|class] [tyres] [username|#]",
                     true,
                     Translation::StringIds::BEST_AVERAGE_OF_3_LAPS),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "pb [car] [tyres] [name]",
                     true,
                     Translation::StringIds::PERSONAL_BESTS),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "stint [name]",
                     true,
                     Translation::StringIds::DRIVER_STINT_INFO),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "tr",
                     false,
                     Translation::StringIds::GET_TRACK_INFO),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "cars",
                     false,
                     Translation::StringIds::SHOW_CUSTOM_CARS_AND_CLASSES_DEFINED),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "opt",
                     false,
                     Translation::StringIds::SET_YOUR_PERSONAL_SETTINGS),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "pl",
                     false,
                     Translation::StringIds::PLAYERS_INFO),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                     "ver",
                     false,
                     Translation::StringIds::VERSION_INFO_AND_SOME_STATS)}),
          mAdminData ({
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                          "cars <car1+car2...>",
                          false,
                          Translation::StringIds::SET_CARS_AVAILABLE_TO_DRIVE),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                          "recent",
                          false,
                          Translation::StringIds::SERVER_RECENT_DRIVERS),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                          "log",
                          false,
                          Translation::StringIds::SHOW_RECENT_CHAT_MESSAGES),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                          "quiet",
                          false,
                          Translation::StringIds::QUIET),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                          "notquiet",
                          false,
                          Translation::StringIds::NOTQUIET),
            std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                          "shutdown",
                          false,
                          Translation::StringIds::SHUT_DOWN_LFSTOP )}),
          mCustomInfoDataProviders (pCustomInfoDataProviders) {
    }

    // INonSwitchableInsimWindowDataProvider interface
    void prepareData () override {
        mCustomInfoCmdData.clear();
        for (auto &el : mCustomInfoDataProviders) {
            const auto help_str = el.second.getHeader();
            if (help_str.size()) {
                mCustomInfoCmdData.push_back(
                    { QByteArray::fromStdString(el.first).prepend("^7!"),
                      help_str });
            }
        }
    }

    size_t getNumRows () const override {
        size_t normal_commands_num = mData.size();
        if (mIsAdmin) {
            normal_commands_num += mAdminData.size() + 1;
        }
        return normal_commands_num;
    }

    size_t getNumColumns () const override {
        return 2;
    }

    void translateCommandParams (QByteArray &pArr,
                                 const Translation::TranslationBinder &pTr) const {
        pArr.replace("car", pTr.get(Translation::StringIds::CAR));
        pArr.replace("class", pTr.get(Translation::StringIds::CLASS));
        pArr.replace("tyres", pTr.get(Translation::StringIds::TYRES));
        pArr.replace("username", pTr.get(Translation::StringIds::USERNAME));
        pArr.replace("name", pTr.get(Translation::StringIds::NAME));
    }

    std::vector<QByteArray> getRowImpl (const std::vector<std::tuple<QByteArray,
                                                      bool,
                                                      Translation::StringIds::Ids>> &pSrc,
                                        const size_t pN,
                                        const Translation::TranslationBinder &pTr) const {
        std::vector<QByteArray> row =
            { std::get<0>(pSrc.at(pN)) };
        if (std::get<1>(pSrc.at(pN))) {
            translateCommandParams(row[0], pTr);
        }
        row.push_back(pTr.get(std::get<2>(pSrc.at(pN))));
        row[0].prepend(mPrefix);
        row[0].prepend("^7");
        return row;
    }

    const std::vector<QByteArray> getRow (const size_t pN,
                                          const Translation::TranslationBinder &pTr) const override {
        const size_t total_data_size = mData.size() + mCustomInfoCmdData.size();
        if (pN >= mData.size()
            && pN - mData.size() < mCustomInfoCmdData.size()) {
            size_t n = pN - mData.size();
            return mCustomInfoCmdData[n];
        } else if (mIsAdmin && pN >= total_data_size) {
            size_t n = pN - total_data_size;

            if (n == 0) {
                return std::vector<QByteArray> {
                    "",
                    QByteArray("^7")
                    + pTr.get(Translation::StringIds::ADMIN_COMMANDS)};
            } else {
                return getRowImpl(mAdminData, n - 1, pTr);
            }
        } else {
            return getRowImpl(mData, pN, pTr);
        }
    }

    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override{
        const unsigned char cmd_w = 40;
        const unsigned char help_w = 58;

        return { { InsimButton::left, cmd_w },
                 { InsimButton::left, help_w }
        };
    }

    void setCommandPrefix (const char pPrefix) {
        mPrefix = pPrefix;
    }

    void setIsAdmin (bool pIsAdmin) {
        mIsAdmin = pIsAdmin;
    }

    void setRecentAdminsOnly (bool pAdminsOnly) {
        // set the help to conform to the data
        if (pAdminsOnly) {
            if (!mRecentAdminsOnly) {
                remove_command_from_help_data(mData, "recent");
                mAdminData.push_back(
                    std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                        "recent",
                        false,
                        Translation::StringIds::SERVER_RECENT_DRIVERS));
                mRecentAdminsOnly = true;
            }
        } else {
            if (mRecentAdminsOnly) {
                remove_command_from_help_data(mAdminData, "recent");
                mData.push_back(
                    std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                        "recent",
                        false,
                        Translation::StringIds::SERVER_RECENT_DRIVERS));
                mRecentAdminsOnly = false;
            }
        }
    }

    void setLogAdminsOnly (bool pAdminsOnly) {
        // set the help to conform to the data
        if (pAdminsOnly) {
            if (!mLogAdminsOnly) {
                remove_command_from_help_data(mData, "log");
                mAdminData.push_back(
                    std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                        "log",
                        false,
                        Translation::StringIds::SHOW_RECENT_CHAT_MESSAGES));
                mLogAdminsOnly = true;
            }
        } else {
            if (mLogAdminsOnly) {
                remove_command_from_help_data(mAdminData, "log");
                mData.push_back(
                    std::make_tuple<QByteArray, bool, Translation::StringIds::Ids>(
                        "log",
                        false,
                        Translation::StringIds::SHOW_RECENT_CHAT_MESSAGES));
                mLogAdminsOnly = false;
            }
        }
    }


private:
    std::vector<std::tuple<QByteArray, bool, Translation::StringIds::Ids>> mData;
    std::vector<std::tuple<QByteArray, bool, Translation::StringIds::Ids>> mAdminData;
    std::map<std::string, CustomInfoDataProvider> &mCustomInfoDataProviders;
    std::vector<std::vector<QByteArray>> mCustomInfoCmdData;
    char mPrefix = '!';
    bool mIsAdmin = false;

    bool mRecentAdminsOnly = true;
    bool mLogAdminsOnly = true;
};

#endif // LFSTOP_HELP_DATA_PROVIDER_HPP_INCLUDED
