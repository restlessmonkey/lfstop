// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "CarsDataProvider.hpp"
#include "Common.hpp"


void CarsDataProvider::setLapsDb (std::shared_ptr<LapsDb> pDb) {
    mDb = pDb;

    mData.clear();

    QMapIterator<QByteArray, CarDefinition > cars_i =
            mDb->getCarsIterator();
    if (cars_i.hasNext()) {
        mData.push_back("^7Cars:");
        while (cars_i.hasNext()) {
            cars_i.next();
            mData.push_back(cars_i.key()
                            + " = "
                            + car_definition_to_string(mDb.get(), cars_i.value()));
        }
    }

    QMapIterator<QByteArray, QSet<QByteArray> > classes_it =
            mDb->getClassesIterator();
    if (classes_it.hasNext()) {
        mData.push_back("^7Classes:");
        while (classes_it.hasNext()) {
            classes_it.next();
            mData.push_back(classes_it.key()
                            + " = "
                            + set_to_string(classes_it.value()));
        }
    }
}
