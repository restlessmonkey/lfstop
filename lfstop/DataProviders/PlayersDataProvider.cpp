// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "PlayersDataProvider.hpp"
#include "ConnectionInfo.hpp"

using Translation::StringIds;
using LfsInsimUtils::insertStr;


void PlayersDataProvider::prepareData () {
    mData.clear();

    int i = 1;
    for (const auto &conn_pair : *mConns) {
        const auto &conn = conn_pair.second;
        if (conn_pair.first == 0) {
            continue;
        }

        QByteArray conn_quality_data;

        if (conn.connectionQualityData.size()
            && (conn.connectionQualityData[0].laggingQuantity
                + conn.connectionQualityData[0].onlineQuantity) > 0) {

            const auto &the_data = conn.connectionQualityData[0];
            float quality =
                (double) the_data.laggingQuantity
                / (double) (the_data.onlineQuantity + the_data.laggingQuantity);

            conn_quality_data = QByteArray::number(quality, 'f', 2) + '%';
        }

        mData.push_back({ QByteArray::number(i),
                          conn.nickname + " ^8(" + conn.username + ')',
                          conn.countryName,
                          conn_quality_data });
        ++i;
    }

}


