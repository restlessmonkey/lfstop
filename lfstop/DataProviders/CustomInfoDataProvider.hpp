// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_CUSTOM_INFO_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_CUSTOM_INFO_DATA_PROVIDER_HPP_INCLUDED


#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QDateTime>


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "LfsInsimUtils.hpp"


#include <limits>


class CustomInfoDataProvider : public INonSwitchableInsimWindowDataProvider {
public:
    CustomInfoDataProvider (QByteArray pFileName)
        : mFileName (pFileName) {
        doPrepareData();
    }

    const QByteArray getHeader () {
        doPrepareData();
        return mHeader;
    }

    // ISwitchableInsimWindowDataProvider interface
    void prepareData () override {
        doPrepareData();
    }
    size_t getNumRows () const override {
        return mData.size();
    }
    size_t getNumColumns () const override {
        return 1;
    }
    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder& pTr) const override {
        (void) pTr;
        return mData.at(pN);
    }
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        return { { InsimButton::left, 98 } };
    }

private:
    void doPrepareData () {
        using LfsInsimUtils::get_closest_space_position;
        QFile file(mFileName);

        QFileInfo file_info(file);
        auto time_stamp = file_info.lastModified().toMSecsSinceEpoch();
        if (time_stamp > mFileTimeStamp) {
            mData.clear();

            if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            QTextStream in(&file);
            mHeader.clear();

            while (!in.atEnd()) {
                QString line = in.readLine();
                if (line.size() > 119) {
                    line = line.left(119);
                }
                const QByteArray data = mDecoder.unicodeStringToLfs(line);

                if (mHeader.isEmpty()) {
                    mHeader = data;
                } else {
                    mData.push_back({ data });
                }
            }
            mFileTimeStamp = time_stamp;
        }
    }


    std::vector<std::vector<QByteArray>> mData;
    const QByteArray mFileName;
    QByteArray mHeader;
    LfsInsimUtils::StringDecoder mDecoder;
    qint64 mFileTimeStamp = std::numeric_limits<qint64>::min();
};


#endif // LFSTOP_CUSTOM_INFO_DATA_PROVIDER_HPP_INCLUDED
