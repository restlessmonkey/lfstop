// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "VersionDataProvider.hpp"
#include "LfsServer.hpp"
#include "LfsTop.hpp"
#include "Common.hpp"
#include "Utils/ProcessMemoryGetter.hpp"
#include "LfsInsimUtils.hpp"
#include "LfsTopVersion.hpp"


VersionDataProvider::VersionDataProvider (LfsServer *pLfsServer,
                                          LFSTop *pLfsTop,
                                          const Translation::Translation &pTr)
    : mLfsServer (pLfsServer),
      mLfsTop (pLfsTop),
      mTr (pTr) {
}

void VersionDataProvider::prepareData () {
    mData.clear();

    using LfsInsimUtils::insertStr;
    using Translation::StringIds;

    Translation::TranslationBinder tr(mTr, mLang);

    QByteArray version = QByteArray("LFSTop v") + LfsTopVersion::as_string;

    const QByteArray git_revision (LfsTopVersion::git_revision);
    if (git_revision.size()) {
        version += ' '
                + tr.get(StringIds::GIT_REVISION)
                + ' '
                + git_revision;
    }

    mData.push_back({ version });
    mData.push_back({ "https://bitbucket.org/restlessmonkey/lfstop" });

    mData.push_back({ "" });

    mData.push_back({
        insertStr(tr.get(StringIds::SERVER_LOCAL_TIME),
                  "^7"
                  + QTime::currentTime().toString(Qt::ISODate).toLatin1()
                  + " ("
                  + QDateTime::currentDateTimeUtc().time()
                  .toString(Qt::ISODate).toLatin1()
                  + " UTC)") });

    mData.push_back({ "" });

    mData.push_back({
        insertStr(tr.get(StringIds::SERVERS),
                  "^7"
                  + QByteArray::number(mLfsTop->getNumServers())) });

    mData.push_back({ "" });

    mData.push_back({ insertStr(tr.get(StringIds::SERVER_CONNECTION_UPTIME),
                                qt_time_stamp_diff_to_str(
                                    mLfsServer->getConnectionUptime(),
                                    tr)) });

    mData.push_back({
        insertStr(tr.get(StringIds::SERVER_BYTES_READ_WRITTEN),
                         QByteArray::number(mLfsServer->getReceivedBytes())
                         + " | "
                         + QByteArray::number(mLfsServer->getSentBytes())) });

    using ProcessMemoryGetter::getCurrentMemoryUsageInKilobytes;
    mData.push_back({
        insertStr(tr.get(StringIds::PROCESS_MEMORY_USAGE_KIB),
                         QByteArray::number((uint)getCurrentMemoryUsageInKilobytes())) });


    if (mLfsTop->getNumServers() > 1) {
        mData.push_back({
            insertStr(tr.get(StringIds::INSTANCE_BYTES_READ_WRITTEN),
                             QByteArray::number(mLfsTop->getReceivedBytes())
                             + " | "
                             + QByteArray::number(mLfsTop->getSentBytes())) });
    }
}
