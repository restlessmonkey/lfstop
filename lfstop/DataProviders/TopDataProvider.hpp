// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_TOP_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_TOP_DATA_PROVIDER_HPP_INCLUDED


#include "ISwitchableInsimWindowDataProvider.hpp"
#include "Databases/LapsDbQuery.hpp"
#include "LfsInsimUtils.hpp"
#include "Translation.hpp"


class UsersDb;
class LapsDb;
struct LapsDbLaptimeAnswer;


class TopDataProvider final: public ISwitchableInsimWindowDataProvider {
public:

    TopDataProvider (const std::shared_ptr<LapsDb> pDb,
                     const UsersDb * const pUsersDb,
                     bool * const pShowSplitsPtr,
                     bool * const pQueryCleanPtr,
                     const Translation::TranslationBinder * const pTr)
        : mUsersDb (pUsersDb),
          mDb (pDb),
          mShowSplits (pShowSplitsPtr),
          mQueryCleanPtr (pQueryCleanPtr),
          mTr (pTr) {
    }

    void setDbName (const QByteArray &pDbName) {
        mDbName = pDbName;
    }

    std::tuple<bool, bool> setQuery (const LapsdbQuery &pQuery);

    // ISwitchableInsimWindowDataProvider interface
    void prepareData () override {
        resetSplitsSectorsSwitch();
        resetCleanNonCleanSwitch();
    }
    size_t getNumRows () const override;

    size_t getNumColumns () const override {
        return 5 - ((mCarHide && mTyreHide) ? 1 : 0);
    }

    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder& pTr) const override;
    const QByteArray getRowColumn (const size_t pRow,
                                   const size_t pColumn) const override;


    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override;

    const QMap<quint8, QByteArray> *getSwitches () const override {
        return &mSwitches;
    }

    std::vector<size_t> switchClicked (const quint8 pSwitchId) override {
        std::vector<size_t> ret;
        if (pSwitchId == 0) {
            ret.push_back(3);
            *mShowSplits = !*mShowSplits;
            resetSplitsSectorsSwitch();
        } else if (pSwitchId == 1) {
            *mQueryCleanPtr = !*mQueryCleanPtr;
            decideIfNonClean(mQuery);
            resetCleanNonCleanSwitch();
            return { };
        }
        return ret;
    }

    bool isExportable () override {
        return true;
    }

    const QString getRowToExport (const size_t pN,
                                  LfsInsimUtils::StringDecoder &pDecoder,
                                  const Translation::TranslationBinder &pTr) const override;

private:
    const QByteArray getAdditionalInfo (const LapsDbLaptimeAnswer &pLt) const;
    const QByteArray getNameFieldColored (const LapsDbLaptimeAnswer &pLt) const;
    const QString getNameFieldNonColored(const LapsDbLaptimeAnswer &pLt,
                            LfsInsimUtils::StringDecoder &pDecoder) const;
    bool decideIfNonClean (const LapsdbQuery &pQuery);

    void resetSplitsSectorsSwitch () {
        using LfsInsimUtils::format;
        using LfsInsimUtils::FormatPair;
        const QByteArray str_to_format = *mShowSplits ? "[ ^7%splits^8 | %sections ]"
                                                      : "[ %splits | ^7%sections^8 ]";

        mSwitches[0] =
            format(str_to_format,
                   FormatPair{ "%splits",
                               mTr->get(Translation::StringIds::SPLITS) },
                   FormatPair{ "%sections",
                               mTr->get(Translation::StringIds::SECTIONS) });
    }

    void resetCleanNonCleanSwitch () {
        using LfsInsimUtils::format;
        using LfsInsimUtils::FormatPair;
        const QByteArray str_to_format = mQueryClean ? "[ ^7%clean^8 | %all ]"
                                                     : "[ %clean | ^7%all^8 ]";

        mSwitches[1] =
            format(str_to_format,
                   FormatPair{ "%clean",
                               mTr->get(Translation::StringIds::CLEAN) },
                   FormatPair{ "%all",
                               mTr->get(Translation::StringIds::ALL) });
    }


    const UsersDb * const mUsersDb;
    const std::shared_ptr<LapsDb> mDb;
    LapsdbQuery mQuery;
    QByteArray mDbName;

    QMap<quint8, QByteArray> mSwitches;

    bool *mShowSplits = nullptr;
    bool *mQueryCleanPtr = nullptr;
    bool mQueryClean = false;

    bool mTyreHide;
    bool mCarHide;

    const Translation::TranslationBinder * const mTr;
};


#endif // LFSTOP_TOP_DATA_PROVIDER_HPP_INCLUDED
