// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "InsimButton.hpp"


#include <cassert>
#include <cstring>
#include <QDebug>


using namespace InsimPacketsUtils;


InsimButton::InsimButton (const QByteArray &pMessage,
                          const unsigned char pRequestId,
                          const unsigned char pLeft,
                          const unsigned char pTop,
                          const unsigned char pWidth,
                          const unsigned char pHeight,
                          const unsigned char pUcid,
                          const unsigned char pClickId) {

    mPacket->Size = 0;
    mPacket->Type = ISP_BTN;
    mPacket->ReqI = pRequestId;
    mPacket->UCID = pUcid;

    mPacket->ClickID = pClickId;
    mPacket->Inst = 0;
    mPacket->BStyle = 0;
    mPacket->TypeIn = 0;

    mPacket->L = pLeft;
    mPacket->T = pTop;
    mPacket->W = pWidth;
    mPacket->H = pHeight;

    setText(pMessage);
}


InsimButton &InsimButton::setAlignment (const InsimButton::Alignment pAlign) {
    switch (pAlign) {
    case left:
        setLeft();
        break;
    case right:
        setRight();
        break;
    case center:
        setFlag(mPacket->BStyle, ISB_RIGHT, false);
        setFlag(mPacket->BStyle, ISB_LEFT, false);
    }
    return *this;
}

InsimButton& InsimButton::setText (const QByteArray &pText) {
    mPacket.setText(pText.constData(), pText.size());
    mPacket->Size = mPacket.getLength();

    return *this;
}










