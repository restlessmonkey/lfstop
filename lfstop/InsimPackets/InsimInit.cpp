// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "InsimInit.hpp"


#include <cstring>
#include <QByteArray>


#include "LfsInsim.hpp"
#include "InsimPacketsUtils.hpp"

using namespace InsimPacketsUtils;

InsimInit::InsimInit (const std::string &pProgramName,
                      const std::string &pPassword) {
    memset(&mPacket, 0, 44);
    mPacket.Size = 44;
    mPacket.Type = ISP_ISI;

    qstrncpy((char*) mPacket.Admin, pPassword.c_str(), 16);
    qstrncpy((char*) mPacket.IName, pProgramName.c_str(), 16);
}

InsimInit & InsimInit::setGetVersion (const bool pGetVersion) {
    mPacket.ReqI = pGetVersion;
    return *this;
}

InsimInit & InsimInit::setUdpPort (const unsigned short pPort) {
    mPacket.UDPPort = pPort;
    return *this;
}

InsimInit & InsimInit::setLocal (const bool pLocal) {
    setFlag(mPacket.Flags, ISF_LOCAL, pLocal);
    return *this;
}

InsimInit & InsimInit::setKeepColors (const bool pKeepColors) {
    setFlag(mPacket.Flags, ISF_MSO_COLS, pKeepColors);
    return *this;
}

InsimInit & InsimInit::setNlp (const bool pNlp) {
    setFlag(mPacket.Flags, ISF_NLP, pNlp);
    return *this;
}

InsimInit & InsimInit::setMci (const bool pMci) {
    setFlag(mPacket.Flags, ISF_MCI, pMci);
    return *this;
}

InsimInit & InsimInit::setCon (const bool pCon) {
    setFlag(mPacket.Flags, ISF_CON, pCon);
    return *this;
}

InsimInit & InsimInit::setObh (const bool pObh) {
    setFlag(mPacket.Flags, ISF_OBH, pObh);
    return *this;
}

InsimInit & InsimInit::setHlv (const bool pHlv) {
    setFlag(mPacket.Flags, ISF_HLV, pHlv);
    return *this;
}

InsimInit & InsimInit::setAxmLoad (const bool pAxmLoad) {
    setFlag(mPacket.Flags, ISF_AXM_LOAD, pAxmLoad);
    return *this;
}

InsimInit & InsimInit::setAxmEdit (const bool pAxmEdit) {
    setFlag(mPacket.Flags, ISF_AXM_EDIT, pAxmEdit);
    return *this;
}

InsimInit & InsimInit::setHostPrefix (const unsigned char pHostPrefix) {
    mPacket.Prefix = pHostPrefix;
    return *this;
}

InsimInit & InsimInit::setInterval (const unsigned short pInterval) {
    mPacket.Interval = pInterval;
    return *this;
}

InsimInit & InsimInit::setProcessJoinRequests (const bool pProcess) {
    setFlag(mPacket.Flags, ISF_REQ_JOIN, pProcess);
    return *this;
}

InsimInit &InsimInit::setVersion (const unsigned char pVersion) {
    mPacket.InSimVer = pVersion;
    return *this;
}

int InsimInit::getLength () const {
    return 44;
}

const char * InsimInit::getPacket() const {
    return (char*) &mPacket;
}
