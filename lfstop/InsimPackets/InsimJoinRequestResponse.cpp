// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "InsimJoinRequestResponse.hpp"


InsimJoinRequestResponse::InsimJoinRequestResponse (const unsigned char pPlid,
                                                    const unsigned char pUcid,
                                                    const unsigned char pAction)
    // if need to set StartPos, write another constructor
    : mPacket (IS_JRR { 16, ISP_JRR, 0, pPlid, pUcid, pAction, 0, 0, {0, 0, 0, 0, 0, 0} }) {
}

const char * InsimJoinRequestResponse::getPacket () const {
    return (char*) &mPacket;
}

int InsimJoinRequestResponse::getLength () const {
    return 16;
}
