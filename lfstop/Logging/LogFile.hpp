// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LOG_FILE_HPP_INCLUDED
#define LFSTOP_LOG_FILE_HPP_INCLUDED


#include <stdexcept>


#include <QFile>
#include <QTextStream>
#include <QMutex>
#include <QMutexLocker>


#include "LogDestination.hpp"

class LogFile final : public LogDestination {
public:
    LogFile (const QString &pFileName)
        : mFile (pFileName) {
    }

protected:
    void logString (const QString &pStr) override {
        QMutexLocker lock(&mMutex);

        if (!mFileIsOpen) {
            if (mFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
                mStream.setDevice(&mFile);
                mFileIsOpen = true;
            } else {
                const std::string what = "can not open file: "
                        + pStr.toStdString();
                throw std::runtime_error(what);
            }
        }
        mStream << pStr << "\n";
        mStream.flush();
    }

private:
    QFile mFile;
    QTextStream mStream;
    QMutex mMutex;
    bool mFileIsOpen = false;
};


#endif // LFSTOP_LOG_FILE_HPP_INCLUDED
