// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LOG_HPP_INCLUDED
#define LFSTOP_LOG_HPP_INCLUDED


#include <QSet>
#include <QTextStream>
#include <QDateTime>


#include "LogDestination.hpp"


#define LOG_DEBUG_PRINT(token) #token << ": " << token


class Log;


class LogSubsection {
public:
    template<typename T>
    LogSubsection & operator<< (const T &pVal) {
        mStream << pVal;
        return *this;
    }

    LogSubsection &operator<< (const std::string &pStr) {
        mStream << QString::fromStdString(pStr);
        return *this;
    }

    LogSubsection (const LogSubsection &pOther)
        : mLevel (pOther.mLevel),
          mMsg (pOther.mMsg),
          mLog (pOther.mLog),
          mStream (&mMsg, QIODevice::WriteOnly) {
    }

    LogSubsection& operator= (const LogSubsection &pOther) = delete;

    ~LogSubsection ();

protected:
    friend class Log;
    LogSubsection (const QString &pLevel, Log &pLog)
        : mLevel (pLevel),
          mLog (pLog),
          mStream (&mMsg, QIODevice::WriteOnly) {
    }

private:
    QString mLevel;
    QString mMsg;
    Log &mLog;
    QTextStream mStream;
};


class Log {
public:
    Log (const QString &pName, LogDestination &pDest, const QString pFormat)
        : mName (pName) {
        mDestinations.insert({ &pDest, pFormat });
    }

    Log (const QString &pName,
         LogDestination &pDest1,
         const QString &pFormat1,
         LogDestination &pDest2,
         const QString &pFormat2)
        : mName (pName) {
        mDestinations.insert({ &pDest1, pFormat1 });
        mDestinations.insert({ &pDest2, pFormat2 });
    }

    explicit Log (const QString &pName)
        : mName (pName) {
    }

    void addDestination (LogDestination &pDest, const QString &pFormat) {
        mDestinations.insert({ &pDest, pFormat });
    }

    enum LogType { INFO, WARNING, ERROR };
    LogSubsection operator() (const LogType pType = INFO) {
        switch (pType) {
        case INFO:
            return LogSubsection(" INFO", *this);
        case WARNING:
            return LogSubsection(" WARN", *this);
        case ERROR:
            return LogSubsection("ERROR", *this);
        }
        return LogSubsection("ERROR", *this);
    }

private:
    friend class LogSubsection;
    QString mName;
    QSet<QPair<LogDestination*, QString>> mDestinations;
};


inline LogSubsection::~LogSubsection() {
    for (QPair<LogDestination*, QString> dst : mLog.mDestinations) {
        QString msg = dst.second;

        msg.replace("%datetime",
                    QDateTime::currentDateTime().toString("dd.MM.yyyy "
                                                          "hh:mm:ss.zzz"));
        msg.replace("%level", mLevel);
        msg.replace("%name", mLog.mName);
        msg.replace("%msg", mMsg);

        dst.first->logString(msg);
    }
}


#endif // LFSTOP_LOG_HPP_INCLUDED
