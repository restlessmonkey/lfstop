#ifndef LFSTOP_I_SWITCHABLE_INSIM_WINDOW_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_I_SWITCHABLE_INSIM_WINDOW_DATA_PROVIDER_HPP_INCLUDED


#include <vector>


#include <QMap>
#include <QByteArray>

#include "InsimPackets/InsimButton.hpp"
#include "InsimWindowColumnFormat.hpp"
#include "LfsInsimUtils.hpp"
#include "Translation.hpp"

class ISwitchableInsimWindowDataProvider {
public:
    virtual void prepareData () = 0;
    virtual size_t getNumRows () const = 0;
    virtual size_t getNumColumns () const = 0;

    virtual const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder& pTr) const = 0;

    virtual const QByteArray getRowColumn (const size_t pRow,
                                           const size_t pColumn) const = 0;
    virtual const std::vector<InsimWindowColumnFormat> getColumnsFormat () const = 0;

    virtual const QMap<quint8, QByteArray> *getSwitches () const = 0;

    // return empty to do nothing, return { -1 } to update completely
    virtual std::vector<size_t> switchClicked (const quint8 pSwitchId) = 0;

    // dirty hax added as a resort to be able to cast votes in TrackVoteDataProvider
    virtual void rowClicked (const size_t pRowNum,
                             const unsigned short pMouseButtonClickFlags) {
        (void) pRowNum;
        (void) pMouseButtonClickFlags;
    }
    virtual bool isInteractive () { return false; }

    virtual bool isExportable () { return false; }
    virtual const QString
    getRowToExport (const size_t pN,
                    LfsInsimUtils::StringDecoder &pDecoder,
                    const Translation::TranslationBinder &pTr) const {
        (void) pN;
        (void) pDecoder;
        (void) pTr;
        return "";
    }
};

#endif // LFSTOP_I_SWITCHABLE_INSIM_WINDOW_DATA_PROVIDER_HPP_INCLUDED
