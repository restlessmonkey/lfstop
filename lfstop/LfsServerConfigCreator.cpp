// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LfsServerConfigCreator.hpp"


#include "Settings.hpp"


using namespace Settings;

namespace {

uint8_t loadClutch (const std::string pClutchTypess, Log &pLog) {
    QByteArray pClutchTypes = QByteArray::fromStdString(pClutchTypess);

    QByteArray clutch_types;
    if (!pClutchTypes.size()) {
        return 7u;
    }


    if (pClutchTypes.contains(' ')) {
        clutch_types = pClutchTypes.split(' ')[0].toUpper();
        pLog(Log::WARNING)
                << "setup_restrictions.clutch parameters ignored: "
                << pClutchTypes
                   .mid(pClutchTypes.indexOf(' '));
    } else {
        clutch_types = pClutchTypes.toUpper();
    }

    if (clutch_types == "ALL") {
        return 7u;
    }

    uint8_t ret = 0u;

    bool is_enabled = true;

    for (int pos = 0; pos < clutch_types.size();) {
        const int next_specifier =
            StringUtils::QByteArrayFindFirstOfTwo(clutch_types, '+', '-', pos);

        const QByteArray token = clutch_types.mid(pos, next_specifier - pos);

        if (token == "ALL") {
            ret = 7u;
        } else if (token == "AUTO") {
            ret ^= (-is_enabled ^ ret) & LfsServerConfigClutchTypes::Auto;
        } else if (token == "AXIS") {
            ret ^= (-is_enabled ^ ret) & LfsServerConfigClutchTypes::Axis;
        } else if (token == "BUTTON") {
            ret ^= (-is_enabled ^ ret) &  LfsServerConfigClutchTypes::Button;
        } else {
            pLog(Log::WARNING) << "setup_restrictions.clutch parameter ignored: "
                               << (is_enabled ? "+" : "-")
                               << token;
        }
        if (next_specifier == -1) {
            break;
        } else if (clutch_types[next_specifier] == '+') {
            is_enabled = true;
        } else {
            is_enabled = false;
        }
        pos = next_specifier + 1;
    }

    return ret;
}


std::string loadCars (std::string pCars, Log &pLog) {
    if (pCars.size()) {
        size_t first_space_pos = pCars.find_first_of(" \t");
        if (first_space_pos != std::string::npos) {
            pLog(Log::WARNING) << "cars parameters ignored: "
                               << pCars.substr(first_space_pos);
            return pCars.substr(0, first_space_pos);
        } else {
            return pCars;
        }
    }
    return "";
}

void enable_all_hlvc (QSet<StintInfo::Lap::ExcludeReason> &pCfg) {
    pCfg.clear();
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_GROUND);
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_WALL);
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_CONTACT);
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_OBJECT_HIT);
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_DRAFT);
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_CAR_RESET);
    pCfg.insert(StintInfo::Lap::ExcludeReason::HLVC_OUT_OF_BOUNDS);
}

QSet<StintInfo::Lap::ExcludeReason> parse_hlvc (const std::string &pHlvcCheckss,
                                                Log &pLog) {
    const QByteArray &pHlvcChecks = QByteArray::fromStdString(pHlvcCheckss);

    QSet<StintInfo::Lap::ExcludeReason> pCfg;

    QByteArray hlvc_config;
    if (pHlvcChecks.size()) {
        if (pHlvcChecks.contains(' ')) {
            hlvc_config = pHlvcChecks.split(' ')[0].toUpper();
            pLog(Log::WARNING) << "hlvc parameters ignored: "
                               << pHlvcChecks.mid(pHlvcChecks.indexOf(' '));
        } else {
            hlvc_config = pHlvcChecks.toUpper();
        }
    }

    if (hlvc_config == "NONE") {
        return {};
    }

    bool is_enabled = true;
    for (int pos = 0; pos < hlvc_config.size();) {
        const int next_specifier =
            StringUtils::QByteArrayFindFirstOfTwo(hlvc_config, '+', '-', pos);

        const QByteArray token = hlvc_config.mid(pos, next_specifier - pos);

        const std::vector<QByteArray> allowed_opts { "GROUND",
                                                     "WALL",
                                                     "CONTACT",
                                                     "OBJECT_HIT",
                                                     "DRAFT"};
        if (token == "ALL") {
            enable_all_hlvc(pCfg);
        } else if (vector_contains(allowed_opts, token)) {
            if (is_enabled) {
                pCfg.insert(StintInfo::Lap::excludeReasonFromString(token));
            } else {
                pCfg.remove(StintInfo::Lap::excludeReasonFromString(token));
            }
        } else {
            pLog(Log::WARNING) << "hlvc_checks parameter ignored: "
                               << (is_enabled ? "+" : "-")
                               << token;
        }
        if (next_specifier == -1) {
            break;
        } else if (hlvc_config[next_specifier] == '+') {
            is_enabled = true;
        } else {
            is_enabled = false;
        }
        pos = next_specifier + 1;
    }

    return pCfg;
}

void parse_hlvc_overrides (std::map<QByteArray,
                           QSet<StintInfo::Lap::ExcludeReason>> &pOverridesMap,
                           const QByteArray &pTracks,
                           const std::string &pHlvcChecks,
                           Log &pLog) {
    QSet<StintInfo::Lap::ExcludeReason> config = parse_hlvc(pHlvcChecks, pLog);

    for (int pos = 0; pos < pTracks.size();) {
        const int next_delimiter = pTracks.indexOf('+', pos);

        const QByteArray token = pTracks.mid(pos, next_delimiter - pos);

        pOverridesMap[token] = config;

        if (next_delimiter == -1) {
            break;
        }
        pos = next_delimiter + 1;
    }
}


std::map<QByteArray, QSet<StintInfo::Lap::ExcludeReason>>
loadHlvcConfigPerTrack (const MKeyFile &pConfigFile, Log& pLog) {
    std::map<QByteArray, QSet<StintInfo::Lap::ExcludeReason>> ret;

    if (pConfigFile.sectionCount("hlvc_override")) {
        auto hlvc_overrides =
                pConfigFile.getLastSectionSettings("hlvc_override");
        while (hlvc_overrides.isElement()) {

            parse_hlvc_overrides(ret,
                                 QByteArray::fromStdString(hlvc_overrides.getName()),
                                 hlvc_overrides.getValue(),
                                 pLog);

            hlvc_overrides.peekNext();
        }
    }

    return ret;
}


std::vector<std::pair<std::string, std::string>>
loadCustomCommands (const MKeyFile &pConfigFile) {
    std::vector<std::pair<std::string, std::string>> ret;

    auto custom_info_commands =
         pConfigFile.getLastSectionSettings("custom_info_commands");

    while (custom_info_commands.isElement()) {
        ret.emplace_back(custom_info_commands.getName(),
                         custom_info_commands.getValue());

        custom_info_commands.peekNext();
    }

    return ret;
}

}

namespace LfsServerConfigLoader {
const LfsServerConfig loadLfsServerConfig (const MKeyFile &pConfigFile,
                                           Log &pLog) {
    SettingsMap cfg = get_settings_map(pConfigFile);

    return LfsServerConfig {
        getValue<std::string>(cfg, "host", "127.0.0.1"),
        getValue<uint16_t>(cfg, "port", 29999),
        getValue<uint16_t>(cfg, "udp_port", 0),
        getValue<std::string>(cfg, "password", ""),

        getValue<bool>(cfg, "setup_restrictions.forbid_traction_control", false),
        getValue<bool>(cfg, "setup_restrictions.forbid_automatic_gears", false),
        loadClutch(getValue<std::string>(cfg, "setup_restrictions.clutch", "ALL"), pLog),

        getValue<char>(cfg, "command_prefix", '!'),

        getValue<bool>(cfg, "recent_admins_only", true),
        getValue<size_t>(cfg, "recent_limit", 100),

        getValue<bool>(cfg, "log_admins_only", true),
        getValue<size_t>(cfg, "log_limit", 100),

        getValue<bool>(cfg, "data_export_enable", false),
        getValue<std::string>(cfg, "data_export_path", ""),
        getValue<std::string>(cfg, "data_export_public_url_prefix", ""),

        getValue<bool>(cfg, "debug_performance", false),
        getValue<bool>(cfg, "debug_insim_packets", false),

        parse_hlvc(getValue<std::string>(cfg, "hlvc", "ALL"), pLog),
        parse_hlvc(getValue<std::string>(cfg, "hlvc_openconfig", "ALL-OBJECT_HIT"), pLog),
        loadHlvcConfigPerTrack(pConfigFile, pLog),

        getValue<bool>(cfg, "track_vote_enable", true),
        getValue<int>(cfg, "track_vote_time", 20),
        getValue<int>(cfg, "track_vote_time_to_cancel", 10),

        loadCars(getValue<std::string>(cfg, "cars", ""), pLog),

        loadCustomCommands(pConfigFile),
        getValue<size_t>(cfg, "stints_num_to_save", 100),
        getValue<size_t>(cfg, "stints_min_num_laps_to_save", 3)
    };
}

} // LfsServerConfigLoader

