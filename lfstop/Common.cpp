// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "Common.hpp"


#include "Utils/StringUtils.hpp"
#include "Utils/StdVectorUtils.hpp"
#include "Databases/LapsDb.hpp"
#include "CompilerUtils.hpp"


std::string format_time_stdstring (const LaptimeT laptime) {
    using StringUtils::toString;
    const unsigned short millisecs = laptime % 1000;
    const unsigned short total_secs = laptime / 1000;
    const unsigned short seconds = total_secs % 60;
    const unsigned short minutes = total_secs / 60 % 60;
    const unsigned short hours = total_secs / 3600;

    std::string ret;

    bool fill_minutes = false;
    if (hours > 0) {
        ret += toString(hours) + ":";
        fill_minutes = true;
    }

    bool fill_seconds = false;
    if (minutes > 0 || hours > 0) {
        if (fill_minutes && minutes < 10) {
            ret += "0";
        }
        ret += toString(minutes) + ":";
        fill_seconds = true;
    }

    if (fill_seconds && seconds < 10) {
        ret += "0";
    }
    ret += toString(seconds) + ".";

    if(millisecs < 100) {
        ret += "0";
    }
    ret += toString((millisecs % 10) == 0 ? millisecs / 10 : millisecs);
    return ret;
}


char digit_to_char_representation (const unsigned char pVal)
LFSTOP_FUNCTION_ATTRIBUTE_CONST;

char digit_to_char_representation (const unsigned char pVal) {
    assert(pVal < 10);
    return '0' + pVal;
}


QByteArray format_time (const LaptimeT laptime) {
    const unsigned short millisecs = laptime % 1000;
    const unsigned short total_secs = laptime / 1000;
    const unsigned char seconds = total_secs % 60;
    const unsigned char minutes = total_secs / 60 % 60;
    const unsigned short hours = total_secs / 3600;

    QByteArray ret(12, '0');

    int pos = 0;

    if (hours) {
        // will be used rarely, so no point to use optimised way
        const QByteArray hours_str = QByteArray::number(hours).append(':');
        ret.insert(0, hours_str);
        pos += hours_str.size();
        if (hours_str.size() > 3) { // will exceed 12 limit
            // extremely rare case
            ret.append("000000000000000", hours_str.size() - 3);
        }
    }

    if (minutes) {
        const unsigned char minutes_first_byte = minutes / 10;
        if (minutes_first_byte || hours) {
            ret[pos++] = digit_to_char_representation(minutes_first_byte);
        }
        ret[pos++] = digit_to_char_representation(minutes % 10);
        ret[pos++] = ':';
    }

    ret[pos++] = digit_to_char_representation(seconds / 10);
    ret[pos++] = digit_to_char_representation(seconds % 10);
    ret[pos++] = '.';

    ret[pos++] = digit_to_char_representation(millisecs / 100);
    ret[pos++] = digit_to_char_representation(millisecs % 100 / 10);
    const unsigned char last_byte = millisecs % 10;
    if (last_byte) {
        ret[pos++] = digit_to_char_representation(last_byte);
    }

    ret.resize(pos);

    return ret;
}

std::string print_scs_stdstring (const LapsDbStorage::Laptime &lap) {
    std::string scs;
    LaptimeT total_sc = 0;

    for (const LaptimeT sp : lap.splits) {
        scs += format_time_stdstring(sp - total_sc) + " ";
        total_sc += sp - total_sc;
    }
    if (lap.splits.size() > 0) {
        scs += format_time_stdstring(lap.laptime - total_sc);
    }
    return scs;
}


QByteArray print_scs (const LapsDbStorage::Laptime &lap, const char sep) {
    QByteArray scs;
    scs.reserve(18);
    LaptimeT total_sc = 0;

    for (const LaptimeT sp : lap.splits) {
        scs += format_time(sp - total_sc).append(sep);
        total_sc += sp - total_sc;
    }

    if (lap.splits.size() > 0) {
        scs += format_time(lap.laptime - total_sc);
    } else {
        scs.chop(1);
    }
    return scs;
}

QByteArray print_sps (const LapsDbStorage::Laptime &lap, const char sep) {
    QByteArray sps;
    sps.reserve(24);

    for (const LaptimeT sp : lap.splits) {
        sps += format_time(sp).append(sep);
    }
    sps.chop(1);
    return sps;
}

QByteArray car_definition_to_string (LapsDb *db, const CarDefinition &def) {
    QByteArray ret_str =
            db->getCarName(def.lfsCar) + " "
            + (def.intakeRestriction ? QByteArray::number(def.intakeRestriction) + "% " : "")
            + (def.addedMass ? QByteArray::number(def.addedMass) + "kg" : "");
    return ret_str;
}

QByteArray set_to_string (const QSet<QByteArray> &set) {
    QByteArray ret_str;
    for (const QByteArray &car : set) {
        ret_str += car + " ";
    }
    return ret_str;
}


QByteArray stb_print_scs (const StintInfo::Lap &pLap,
                          const std::vector<size_t> &pHighlightScs,
                          const QByteArray &pRevertTo) {
    QByteArray scs;

    for (size_t i = 0; i < pLap.sections.size(); ++i) {
        scs += (vector_contains(pHighlightScs, i) ? "^2" : "")
                + format_time(pLap.sections[i])
                + (vector_contains(pHighlightScs, i) ? pRevertTo : "")
                + " ";
    }

    return scs;
}

QByteArray stb_print_scs (const StintInfo::Lap &pLap,
                          const char pSeparator) {
    QByteArray scs;

    for (const LaptimeT sc : pLap.sections) {
        scs += format_time(sc) + pSeparator;
    }

    if (scs.size()) {
        scs.chop(1);
    }

    return scs;
}


QByteArray qt_time_stamp_diff_to_str (const qint64 pDiff,
                                      const Translation::TranslationBinder & pTr) {
    const qint64 total_secs = pDiff / 1000;
    const short seconds = total_secs % 60;
    const short minutes = total_secs / 60 % 60;
    const qint64 total_hours = total_secs / 3600;

    const short hours = total_hours % 24;
    const short days = total_hours / 24;

    QByteArray ret;

    using Translation::StringIds;

    if (days) {
        ret += QByteArray::number(days) + ' '
                +  pTr.get(StringIds::DAYS) + ' ';
    }

    if (hours || days) {
        ret += QByteArray::number(total_hours) + ' '
                +  pTr.get(StringIds::HOURS) + ' ';
    }

    ret += QByteArray::number(minutes) + ' '
            +  pTr.get(StringIds::MINUTES) + ' ';
    ret += QByteArray::number(seconds) + ' '
            +  pTr.get(StringIds::SECONDS);

    return ret;
}

std::vector<LaptimeT> stint_sections_to_splits (std::vector<LaptimeT> pVec) {
    std::vector<LaptimeT> ret;

    for (size_t i = 0; i < pVec.size() - 1; ++i) {
        ret.push_back(pVec[i]);
        if (ret.size() > 1) {
            ret.back() += ret[ret.size() - 2];
        }
    }
    return ret;
}
