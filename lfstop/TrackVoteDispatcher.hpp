// Copyright 2015, 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_TRACK_VOTE_DISPATCHER_HPP_INCLUDE
#define LFSTOP_TRACK_VOTE_DISPATCHER_HPP_INCLUDE

#include <vector>
#include <QByteArray>
#include <utility>
#include <chrono>
#include <random>
#include <cassert>

#include "Databases/Fundamentals/Tracks.hpp"

class TrackVoteDispatcher {
    static QByteArray metersPrintLikeKilometers (int pMeters) {
        QByteArray ret;
        int kilometers = pMeters / 1000;
        ret = QByteArray::number(kilometers);
        ret += '.';
        int meters_left = pMeters % 1000;
        if (meters_left < 100) {
            ret += '0';
            if(meters_left < 10) {
                ret += '0';
            }
        }
        ret += QByteArray::number(meters_left);
        return ret;
    }

public:
    enum class Mode : unsigned char { CLOSED, SITE, TRACK } currentMode
        = Mode::CLOSED;
    size_t selectedSite = 0;
    size_t selectedTrack = 0;

    std::vector<size_t> updatedVotes;
    size_t totalVotesCast = 0;

    std::vector<std::vector<QByteArray>> currentPrintableFields; // names to print
    std::vector<size_t> votableElementIds; // element ids in the Tracks::
                                           // (or layouts when/if they supported)
                                           // that we can vote for
    std::vector<size_t> currentVotes; // id in votableElementIds

    TrackVoteDispatcher ()
        : mRandomEngine (static_cast<unsigned long>(std::chrono::system_clock::now()
                             .time_since_epoch()
                             .count())) {
    }

    // to be called before track and site voting starts to take place
    void prepareData () {
        currentPrintableFields.clear();
        votableElementIds.clear();
        currentVotes.clear();

        if (currentMode == Mode::SITE) {
            for (size_t i = 0; i < Tracks::lfsTrackSites.size(); ++i) {
                currentPrintableFields.push_back(
                    { Tracks::lfsTracksSitesLists[i].first.shortName,
                      Tracks::lfsTracksSitesLists[i].first.longName,
                      QByteArray::number((int)Tracks::lfsTracksSitesLists[i]
                                             .second
                                             .size())
                      + " configs",
                      "0" });
                votableElementIds.push_back(i);
            }
            currentVotes.resize(votableElementIds.size());
        } else if (currentMode == Mode::TRACK) {
            assert(selectedSite != std::numeric_limits<size_t>::max());
            for (size_t i = 0;
                 i < Tracks::lfsTracksSitesLists[selectedSite].second.size();
                 ++i) {
                auto &el = Tracks::lfsTracksSitesLists[selectedSite].second[i];
                currentPrintableFields.push_back(
                    { el.shortName,
                      el.longName,
                      metersPrintLikeKilometers(el.lengthMeters) + "km",
                      QByteArray::number(el.gridSize) + " slots",
                      "0", });
                votableElementIds.push_back(i);
            }
            currentVotes.resize(votableElementIds.size());
        }
    }

    void concludeVote () {
        assert(currentMode != Mode::CLOSED);

        // stores number in votableElementIds, not the number of votes
        std::vector<size_t> maxResults { 0 };

        for (size_t i = 1; i < votableElementIds.size(); ++i) {
            if (currentVotes[i] > currentVotes[maxResults[0]]) {
                maxResults.clear();
                maxResults.push_back(i);
            } else if (currentVotes[i] == currentVotes[maxResults[0]]) {
                maxResults.push_back(i);
            }
        }

        if (maxResults.size() > 1) {
            std::uniform_int_distribution<size_t>
                rnd_res(0, static_cast<size_t>(maxResults.size()) - 1);
            if (currentMode == Mode::SITE) {
                selectedSite = votableElementIds[maxResults[rnd_res(mRandomEngine)]];
            } else {
                selectedTrack = votableElementIds[maxResults[rnd_res(mRandomEngine)]];
            }
        } else {
            if (currentMode == Mode::SITE) {
                selectedSite = votableElementIds[maxResults[0]];
            } else {
                selectedTrack = votableElementIds[maxResults[0]];
            }
        }
    }

private:
    std::mt19937 mRandomEngine;
};


#endif // LFSTOP_TRACK_VOTE_DISPATCHER_HPP_INCLUDE
