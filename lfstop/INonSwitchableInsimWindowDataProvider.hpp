#ifndef LFSTOP_I_NON_SWITCHABLE_INSIM_WINDOW_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_I_NON_SWITCHABLE_INSIM_WINDOW_DATA_PROVIDER_HPP_INCLUDED


#include <QMap>
#include <QByteArray>


#include "ISwitchableInsimWindowDataProvider.hpp"


class INonSwitchableInsimWindowDataProvider : public ISwitchableInsimWindowDataProvider {
public:
    virtual const QByteArray getRowColumn (const size_t pRow,
                                           const size_t pColumn) const override {
        (void) pRow;
        (void) pColumn;
        return "";
    }

    virtual const QMap<quint8, QByteArray> *getSwitches () const override final{
        return {};
    }
    virtual std::vector<size_t> switchClicked (const quint8 pSwitchId) override final{
        (void) pSwitchId;
        return {};
    }
};

#endif // LFSTOP_I_NON_SWITCHABLE_INSIM_WINDOW_DATA_PROVIDER_HPP_INCLUDED
