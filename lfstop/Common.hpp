// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_COMMON_HPP_INCLUDED
#define LFSTOP_COMMON_HPP_INCLUDED


#include <QByteArray>


#include <string>


#include "Databases/LapsDbStorage.hpp"
#include "StintInfo.hpp"
#include "Translation.hpp"


class LapsDb;


std::vector<LaptimeT> stint_sections_to_splits (std::vector<LaptimeT> pVec);

std::string format_time_stdstring (const LaptimeT laptime);
std::string print_scs_stdstring (const LapsDbStorage::Laptime &lap);

QByteArray format_time (const LaptimeT laptime);
QByteArray print_scs (const LapsDbStorage::Laptime &lap, const char sep = ' ');
QByteArray print_sps (const LapsDbStorage::Laptime &lap, const char sep = ' ');

QByteArray qt_time_stamp_diff_to_str (const qint64 pDiff,
                                      const Translation::TranslationBinder &pTr);

QByteArray car_definition_to_string (LapsDb *db, const CarDefinition &def);

QByteArray set_to_string (const QSet<QByteArray> &set);

QByteArray print_tyre_compound (const TyreT tyre);

QByteArray stb_print_scs (const StintInfo::Lap &pLap,
                          const std::vector<size_t> &pHighlightScs,
                          const QByteArray &pRevertTo);

QByteArray stb_print_scs (const StintInfo::Lap &pLap,
                          const char pSeparator = ' ');


#endif // LFSTOP_COMMON_HPP_INCLUDED
