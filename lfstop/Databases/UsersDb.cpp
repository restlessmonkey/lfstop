// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "UsersDb.hpp"


#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QDataStream>


#include "LfsInsimUtils.hpp"
#include "Utils/StringUtils.hpp"


void load_db_v1 (QDataStream &in, UsersDbStorage &db);
void load_db_v2 (QDataStream &in, UsersDbStorage &db);

void write_db (QDataStream &out, const UsersDbStorage &db);


UsersDb::UsersDb (Log &pLog)
    : mLog (pLog) {
    QFile file("Users.dat");
    const QString abs_path = QFileInfo(file.fileName()).absoluteFilePath();

    if (file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);
        QByteArray file_magic_string;
        qint32 actual_file_format_version;
        in >> file_magic_string;
        in >> actual_file_format_version;

        if (file_magic_string != "LfsTopUsersDb") {
            mLog(Log::WARNING) << "wrong file format: " << abs_path;
            mLog(Log::WARNING) << "moving file to: " << abs_path << ".invalid";
            if (!file.rename("Users.dat.invalid")) {
                mLog(Log::ERROR) << "failed to rename file: " << abs_path << ".invalid";
                throw std::runtime_error("failed to rename file");
            }

        } else if (actual_file_format_version != mDbFileFormatVersion) {
            if (actual_file_format_version < mDbFileFormatVersion) {
                mLog(Log::INFO) << "will probably import users from previous "
                                   "version database file: " << abs_path;
                mLog(Log::INFO) << "making backup file: " << abs_path
                                   << ".old" << actual_file_format_version;
                file.copy("Users.dat.old" +
                            QByteArray::number(actual_file_format_version));

                if (actual_file_format_version == 1) {
                    mLog(Log::INFO) << "importing users from previous"
                                       "version database file: " << abs_path;
                    load_db_v1(in, mDb);
                    mNeedToSave = true;
                }
            } else {
                mLog(Log::WARNING) << "moving future file format version to: "
                                   << abs_path
                                   << ".new" << actual_file_format_version;
                const auto newname = "Users.dat.new" +
                        QByteArray::number(actual_file_format_version);
                if (!file.rename(newname)) {
                    mLog(Log::ERROR) << "failed to rename file: " << abs_path << ".invalid";
                    throw std::runtime_error("failed to rename file");
                }
            }
        } else {
            load_db_v2(in, mDb);
            mLog() << "Users database loaded";
        }
    }
    else {
            mLog(Log::WARNING) << "Could not read Users database: " << abs_path
                               << ", proceeding with empty";
    }
}


UsersDb::~UsersDb () {
    if (mNeedToSave) {
        QFile file("Users.dat");
        if (file.open(QIODevice::WriteOnly)) {
            QDataStream out(&file);
            out << QByteArray("LfsTopUsersDb") << mDbFileFormatVersion;
            write_db(out, mDb);
            mLog() << "Users database saved";
        }
        else {
            QFileInfo file_info(file.fileName());
            mLog(Log::ERROR) << "Could not write Users database: "
                             << file_info.absoluteFilePath();
        }
    }
}


UsersDbStorage::UserSettings UsersDb::userGetSettings (const QByteArray &pUser) {
    QReadLocker locker(&mLock);
    QMapIterator<UsersDbStorage::Username,
                 UsersDbStorage::UserData> it(mDb.users);
    while(it.hasNext()) {
        it.next();
        if(it.key().data.toLower() == pUser.toLower()) {
            return it.value().settings;
        }
    }

    return UsersDbStorage::UserSettings();
}

bool UsersDb::userResetCase (const QByteArray &pUser) {
    QMapIterator<UsersDbStorage::Username, UsersDbStorage::UserData> it(mDb.users);
    while(it.hasNext()) {
        it.next();
        if(it.key().data.toLower() == pUser.toLower()) {
            const UsersDbStorage::UserData copy = it.value();
            mDb.users.remove(it.key());
            mDb.users[{pUser}] = copy;
            return true;
        }
    }
    return false;
}


void UsersDb::userSetNickname (const QByteArray &pUser,
                               const QByteArray &pNickname) {
    QWriteLocker locker(&mLock);

    if (userResetCase(pUser)) {
        mNeedToSave = true;
    }

    if((mDb.users.contains(pUser) && mDb.users[pUser].nickname != pNickname)
       || !mDb.users.contains(pUser)) {
        mDb.users[pUser].nickname = pNickname;
        mNeedToSave = true;
    }
}


QByteArray UsersDb::userGetNickname (const QByteArray &pUser) const {
    QReadLocker locker(&mLock);

    const QMap<UsersDbStorage::Username, UsersDbStorage::UserData>::const_iterator it =
            mDb.users.lowerBound(pUser);

    if (StringUtils::compareCaseInsensitive(it.key().data, pUser)) {
        return it.value().nickname;
    }

    return "";
}

QByteArray UsersDb::userGetFullName (const QByteArray &pUsername) const {
    QReadLocker locker(&mLock);

    const QMap<UsersDbStorage::Username, UsersDbStorage::UserData>::const_iterator it =
            mDb.users.lowerBound(pUsername);

    if (StringUtils::compareCaseInsensitive(it.key().data, pUsername)
        && it.value().nickname != pUsername) {
        QByteArray ret;
        ret.reserve(pUsername.size() + it.value().nickname.size() + 2 + 1 + 2);
        ret = it.value().nickname + QByteArray(" ^8(", 4) + pUsername + ')';

        return ret;
    }

    return pUsername;
}

const QByteArray UsersDb::findUsernameByString (const QByteArray &pString) const {
    QReadLocker locker(&mLock);

    using LfsInsimUtils::stringNonEscapedNonColored;

    // make sure we return proper stuff when we have complete username match
    QMapIterator<UsersDbStorage::Username, UsersDbStorage::UserData> it (mDb.users);
    while (it.hasNext()) {
        it.next();
        if (stringNonEscapedNonColored(it.key().data).toUpper()
            == pString.toUpper()) {
            return it.key().data;
        }
    }

    it.toFront();
    while (it.hasNext()) {
        it.next();
        if (stringNonEscapedNonColored(it.key().data).toUpper()
                .contains(stringNonEscapedNonColored(pString).toUpper())
            || stringNonEscapedNonColored(it.value().nickname).toUpper()
                .contains(stringNonEscapedNonColored(pString).toUpper())) {
            return it.key().data;
        }
    }

    return QByteArray("");
}

void UsersDb::userSetSettings (const QByteArray &pUser,
                               const UsersDbStorage::UserSettings &pUserSettings) {
    QWriteLocker locker(&mLock);

    userResetCase(pUser);
    mDb.users[pUser].settings = pUserSettings;
    mNeedToSave = true;
}


void load_db_v1 (QDataStream &in, UsersDbStorage &db) {
    QDataStream::Status oldStatus = in.status();
    in.resetStatus();

    db.users.clear();

    quint32 n;
    in >> n;

    db.users.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (in.status() != QDataStream::Ok) {
            break;
        }

        UsersDbStorage::Username un;
        UsersDbStorage::UserData ud;

        in >> un.data;
        in >> ud.nickname;
        in >> ud.settings.switches;
        ud.settings.language = "?";

        db.users.insertMulti(un, ud);
    }

    if (in.status() != QDataStream::Ok) {
        db.users.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        in.setStatus(oldStatus);
    }
}


void load_db_v2 (QDataStream &in, UsersDbStorage &db) {
    QDataStream::Status oldStatus = in.status();
    in.resetStatus();

    db.users.clear();

    quint32 n;
    in >> n;

    db.users.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (in.status() != QDataStream::Ok) {
            break;
        }

        UsersDbStorage::Username un;
        UsersDbStorage::UserData ud;

        in >> un.data
           >> ud.nickname
           >> ud.settings.switches
           >> ud.settings.language;

        db.users.insertMulti(un, ud);
    }

    if (in.status() != QDataStream::Ok) {
        db.users.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        in.setStatus(oldStatus);
    }
}

void write_db (QDataStream &out, const UsersDbStorage &db) {

    out << quint32(db.users.size());

    QMap<UsersDbStorage::Username, UsersDbStorage::UserData>::ConstIterator it
            = db.users.end();

    const QMap<UsersDbStorage::Username,
               UsersDbStorage::UserData>::ConstIterator begin
            = db.users.begin();

    while (it != begin) {
        --it;
		// PVS: V807 Decreased performance. Consider creating a reference to avoid using the 'it.value()' expression repeatedly. usersdb.cpp 296
        out << it.key().data
            << it.value().nickname
            << it.value().settings.switches
            << it.value().settings.language;
    }
}
