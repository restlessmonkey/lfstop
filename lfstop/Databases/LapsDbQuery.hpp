// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LAPS_DB_QUERY_HPP_INCLUDED
#define LFSTOP_LAPS_DB_QUERY_HPP_INCLUDED


#include "Fundamentals/CarT.hpp"
#include "Fundamentals/ClassT.hpp"
#include "Fundamentals/TyreT.hpp"


struct LapsdbQuery {
    LapsdbQuery () {}
    LapsdbQuery (const CarT pCar, const ClassT pClass, const TyreT pTyres)
        : car (pCar),
          carsClass (pClass),
          tyres (pTyres) {
    }
    CarT car;
    ClassT carsClass = ClassT::notFound();
    TyreT tyres;
};


#endif // LFSTOP_LAPS_DB_QUERY_HPP_INCLUDED
