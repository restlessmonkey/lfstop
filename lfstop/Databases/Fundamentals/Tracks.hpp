// Copyright 2015, 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <map>
#include <string>
#include <vector>
#include <QByteArray>


class Tracks {
public:
    static const std::vector<std::pair<std::string, size_t>> lfsTrackSites;

    struct SiteInfo {
        QByteArray shortName;
        QByteArray longName;
    };

    struct TrackInfo {
        QByteArray shortName;
        QByteArray longName;
        int lengthMeters;
        int gridSize;
    };

    enum class TrackFlags : char {
        CAR_PARK = 0,
        HAS_REVERSE_CONFIG = 1,
        CAN_GRID_AND_SECTIONS = 2,
        ONEWAY_RACING_TRACK = 2,
        TWOWAY_RACING_TRACK = 2 | 1,
    };

    static const std::vector<std::pair<SiteInfo, std::vector<TrackInfo>>> lfsTracksSitesLists;
};

