// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_FUNDAMENTALS_CLASS_T_HPP_INCLUDED
#define LFSTOP_FUNDAMENTALS_CLASS_T_HPP_INCLUDED


#include <cstdint>
#include <vector>


#include <QDataStream>


#include "Databases/Fundamentals/TypeSafetyWrapper.hpp"
#include "Utils/StdVectorUtils.hpp"


class ClassT final : public TypeSafetyWrapper<uint16_t, ClassT> {
public:
    using base = TypeSafetyWrapper<uint16_t, ClassT>;
    using base::base;

    inline constexpr ClassT ()
        : base (0) {
    }

    static inline constexpr ClassT notFound () {
        return ClassT(std::numeric_limits<ClassT::InnerType>::max());
    }
};

inline uint qHash (const ClassT &pClass, uint seed) {
    return qHash(pClass.value, seed);
}

inline QDataStream &operator<< (QDataStream &out, const ClassT &c) {
    out << c.value;
    return out;
}

inline QDataStream &operator>> (QDataStream &in, ClassT &c) {
    in >> c.value;
    return in;
}


#endif // LFSTOP_FUNDAMENTALS_CLASS_T_HPP_INCLUDED
