
// Copyright 2015, 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "Tracks.hpp"


const std::vector<std::pair<std::string, size_t>> Tracks::lfsTrackSites =
    { { "BL", 3 },
      { "SO", 6 },
      { "FE", 6 },
      { "AU", 4 },
      { "KY", 3 },
      { "WE", 5 },
      { "AS", 7 },
      { "RO", 11},
    };

const std::vector<std::pair<Tracks::SiteInfo, std::vector<Tracks::TrackInfo>>>
Tracks::lfsTracksSitesLists =

{ { { "BL", "Blackwood" },
    { { "BL1",  "GP Track",                     3307, 32 },
      { "BL1R", "GP Track Reversed",            3307, 32 },
      { "BL2",  "Rallycross",                   1839, 30 },
      { "BL2R", "Rallycross Reversed",          1839, 30 },
      { "BL3",  "Car park",                     0,    16 },
  } },
  { { "SO", "South City" },
    { { "SO1",  "Classic",                      2033, 30 },
      { "SO1R", "Classic Reversed",             2033, 30 },
      { "SO2",  "Sprint 1",                     2048, 16 },
      { "SO2R", "Sprint 1 Reversed",            2048, 16 },
      { "SO3",  "Sprint 2",                     1334, 16 },
      { "SO3R", "Sprint 2 Reversed",            1334, 16 },
      { "SO4",  "City Long",                    4029, 32 },
      { "SO4R", "City Long Reversed",           4029, 32 },
      { "SO5",  "Town Course",                  3146, 32 },
      { "SO5R", "Town Course Reversed",         3146, 32 },
      { "SO6",  "Chicane Route",                2917, 32 },
      { "SO6R", "Chicane Route Reversed",       2917, 32 },
    } },
  { { "FE", "Fern Bay" },
    { { "FE1",  "Club",                         1584, 32 },
      { "FE1R", "Club Reversed",                1584, 32 },
      { "FE2",  "Green",                        3086, 32 },
      { "FE2R", "Green Reversed",               3086, 32 },
      { "FE3",  "Gold",                         3514, 32 },
      { "FE3R", "Gold Reversed",                3514, 32 },
      { "FE4",  "Black",                        6559, 32 },
      { "FE4R", "Black Reversed",               6559, 32 },
      { "FE5",  "Rallycross",                   2018, 32 },
      { "FE5R", "Rallycross Reversed",          2018, 32 },
      { "FE6",  "Rallycross Green",             745,  24 },
      { "FE6R", "Rallycross Green Reversed",    745,  24 },
    } },
  { { "AU", "Autocross" },
    { { "AU1",  "",                             0,    16 },
      { "AU2",  "Skid Pad",                     0,    16 },
      { "AU3",  "Drag Strip",                   402,  2 },
      { "AU4",  "8 Lane Drap",                  402,  8 },
    } },
  { { "KY", "Kyoto Ring" },
    { { "KY1",  "Oval",                         2980, 32 },
      { "KY1R", "Oval Reversed",                2980, 32 },
      { "KY2",  "National",                     5138, 32 },
      { "KY2R", "National Reversed",            5138, 32 },
      { "KY3",  "GP Long",                      7377, 32 },
      { "KY3R", "GP Long Reversed",             7377, 32 },
    } },
  { { "WE", "Westhill" },
    { { "WE1",  "National",                     4396, 40 },
      { "WE1R", "National Reversed",            4396, 40 },
      { "WE2",  "International",                5750, 40 },
      { "WE2R", "International Reversed",       5750, 40 },
      { "WE3",  "Car Park",                     0,    40 },
      { "WE4",  "Karting",                      483,  40 },
      { "WE4R", "Karting Reversed",             483,  40 },
      { "WE5",  "Karting National",             1316, 40 },
      { "WE5R", "Karting National Reversed",    1316, 40 },
    } },
  { { "AS", "Aston" },
    { { "AS1",  "Cadet",                        1870, 32 },
      { "AS1R", "Cadet Reversed",               1870, 32 },
      { "AS2",  "Club",                         3077, 32 },
      { "AS2R", "Club Reversed",                3077, 32 },
      { "AS3",  "National",                     5602, 32 },
      { "AS3R", "National Reversed",            5602, 32 },
      { "AS4",  "Historic",                     8089, 32 },
      { "AS4R", "Historic Reversed",            8089, 32 },
      { "AS5",  "Grand Prix",                   8802, 32 },
      { "AS5R", "Grand Prix Reversed",          8802, 32 },
      { "AS6",  "Grand Touring",                8002, 32 },
      { "AS6R", "Grand Touring Reversed",       8002, 32 },
      { "AS7",  "North",                        5168, 32 },
      { "AS7R", "North Reversed",               5168, 32 },
    } },
  { { "RO", "Rockingham" },
    { { "RO1",  "ISSC",                         3097, 40 },
      { "RO2",  "National",                     2697, 40 },
      { "RO3",  "Oval",                         2363, 40 },
      { "RO4",  "ISSC Long",                    3253, 40 },
      { "RO5",  "Lake",                         1046, 40 },
      { "RO6",  "Handling",                     1559, 40 },
      { "RO7",  "International",                3874, 40 },
      { "RO8",  "Historic",                     3565, 40 },
      { "RO9",  "Historic Short",               2197, 40 },
      { "RO10", "International Long",           4056, 40 },
      { "RO11", "Sportscar",                    2693, 40 },
    } },
};

