// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_STINTS_DB_HPP_INCLUDED
#define LFSTOP_STINTS_DB_HPP_INCLUDED


#include <list>


#include <QByteArray>


#include "Databases/Fundamentals/CarT.hpp"
#include "Databases/Fundamentals/TyreT.hpp"
#include "StintInfo.hpp"
#include "LapsDbStorage.hpp"


struct StintInfoListRow {
    QByteArray username;
    CarT car;
    TyreT tyres;
    StintInfo stintInfo;
    qint64 timeStamp;
};


using StintInfoList = std::list<StintInfoListRow>;


class StintsDb {
public:
    StintsDb (QByteArray pName,
              Log &pLog);

    ~StintsDb ();

    StintInfoList stintsList;

private:
    QByteArray mFileName;
    Log &mLog;

    static constexpr qint32 mFileFormatVersion = 1;
};


QDataStream &operator<< (QDataStream &out, const StintInfo::Lap &lap);
QDataStream &operator>> (QDataStream &in, StintInfo::Lap &lap);

QDataStream &operator<< (QDataStream &out, const  StintInfo &inf);
QDataStream &operator>> (QDataStream &in, StintInfo &inf);

QDataStream &operator<< (QDataStream &out, const  StintInfoListRow &row);
QDataStream &operator>> (QDataStream &in, StintInfoListRow &row);


template <typename T>
QDataStream &operator<< (QDataStream &out, const std::vector<T> &l) {
    out << quint32(l.size());
    for (auto &el : l) {
        out << el;
    }
    return out;
}

template <typename T>
QDataStream &operator>> (QDataStream &in, std::vector<T> &l) {
    l.clear();
    quint32 c;
    in >> c;
    l.reserve(c);
    for (quint32 i = 0; i < c; ++i) {
        T el;
        in >> el;
        l.push_back(el);
        if (in.atEnd()) {
            break;
        }
    }
    return in;
}



template <typename T>
QDataStream &operator<< (QDataStream &out, const std::list<T> &l) {
    out << quint32(l.size());
    for (auto &el : l) {
        out << el;
    }
    return out;
}

template <typename T>
QDataStream &operator>> (QDataStream &in, std::list<T> &l) {
    l.clear();
    quint32 c;
    in >> c;
    for (quint32 i = 0; i < c; ++i) {
        T el;
        in >> el;
        l.push_back(el);
        if (in.atEnd()) {
            break;
        }
    }
    return in;
}



#endif // LFSTOP_STINTS_DB_HPP_INCLUDED
