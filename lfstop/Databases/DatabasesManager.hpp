// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_DATABASES_MANAGER_HPP_INCLUDED
#define LFSTOP_DATABASES_MANAGER_HPP_INCLUDED


#include <string>
#include <memory>


#include <QObject>
#include <QMutex>


#include "ip2location/IP2Location.h"


#include "Databases/LapsDb.hpp"
#include "Databases/WrDb.hpp"
#include "Utils/MKeyFile.hpp"
#include "Settings.hpp"


class LfsDatabasesManager : public QObject {
    Q_OBJECT
public slots:
    void fillWrs ();
public:
    LfsDatabasesManager (Log &pLog,
                         const MKeyFile &pConfigFile,
                         const Settings::SettingsMap &pSettings,
                         const bool pDisableWr = false);
    ~LfsDatabasesManager ();
    std::shared_ptr<LapsDb> getDatabase (const std::string &pTrackName);
    void freeDatabase (const std::string &pTrackName);

    QByteArray getIpLocation (const unsigned pIp);

private slots:
    void saveDatabases();

private:
    void loadCarsAndClasses ();
    LapsDb * loadDatabase (const std::string &pTrackName);
    void fillTrackWrs (const std::string &pTrackName);

    // trackname : prefix : usage_count
    QMap <std::string, int> mTrackUsageCounters;
    // trackname : trackname_prefix : db
    QMap <std::string, std::shared_ptr<LapsDb>> mTrackDatabases;


    QMutex mMutex;
    QMutex mIpLocationMutex;

    const MKeyFile &mConfigFile;
    const Settings::SettingsMap &mSettingsMap;

    Log &mLog;

    WrDb mWrDb;

    QMap <QByteArray, CarDefinition> mCustomCars;
    QMap <QByteArray, QSet<QByteArray>> mClasses;

    QTimer mDatabasesSaveTimer;
    static constexpr int mSaveIntervalMins = 1;

    IP2Location *mIp2Location = nullptr;
};

#endif // LFSTOP_DATABASES_MANAGER_HPP_INCLUDED
