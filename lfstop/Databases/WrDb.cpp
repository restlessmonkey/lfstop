// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "WrDb.hpp"


#include <QFile>
#include <QFileInfo>
#include <QNetworkAccessManager>
#include <QNetworkReply>


#include "Utils/StringUtils.hpp"
#include "Utils/StdVectorUtils.hpp"

#include "Databases/Fundamentals/Tracks.hpp"


std::string pubstat_track_to_normal (const std::string &pTrack) {
    //std::vector<std::string> lfs_tracks
    // = { "BL", "SO", "FE", "AU", "KY", "WE", "AS", "RO" };

    std::string track;
    size_t n = StringUtils::fromString<size_t>(pTrack.substr(0, 1));
    if (Tracks::lfsTrackSites.size() > n) {
        track = Tracks::lfsTrackSites[n].first;
    } else {
        track = "??";
    }

    track += StringUtils::toString(StringUtils::fromString<unsigned>(pTrack.substr(1, 1)) + 1);

    if (pTrack.substr(2, 1) == "1") {
        track += "R";
    }

    return track;
}


void WrDb::processReply (QNetworkReply *pReply) {
    QByteArray data = pReply->readAll();

    QList<QByteArray> wrs = data.split('\n');
    if (wrs[0].startsWith("no output")) {
        mLog(Log::WARNING) << "WR update failed: no output from lfsworld";
        return;
    }
    else if (wrs[0].startsWith("Invalid Ident-Key")) {
        mLog(Log::WARNING) << "WR update failed: invalid ident key";
        return;
    }
    for (const QByteArray &wr : wrs) {
        // <id> <track> <car> <sp1> <sp2> <sp3> <time> <flags> <name> <date>

        StringUtils::Tokenizer tokens (wr.toStdString());
        const size_t tokens_num = tokens.getNumWords();
        if (tokens_num < 10) {
            continue;
        }

        using StringUtils::fromString;

        const QByteArray track =
                QByteArray::fromStdString(pubstat_track_to_normal(tokens.getWord(1)));
//        const CarT car = vector_find(LapsdbTypeInfo::cars,
//                                     QByteArray::fromStdString(tokens.getWord(2)));
        const CarT car =
                CarT::lfsCarFromString(QByteArray::fromStdString(tokens.getWord(2)));
        const LaptimeT sp1 = fromString<LaptimeT>(tokens.getWord(3));
        const LaptimeT sp2 = fromString<LaptimeT>(tokens.getWord(4));
        const LaptimeT sp3 = fromString<LaptimeT>(tokens.getWord(5));
        const LaptimeT laptime = fromString<LaptimeT>(tokens.getWord(6));
        const std::string username = tokens.getWords(8, tokens_num - 8 - 1);
        const qint64 timestamp = fromString<qint64>(tokens.getWord(tokens_num - 1));

        LapsDbStorage::Laptime lt(timestamp);
        if (sp1) {
            lt.splits.append(sp1);
            if (sp2) {
                lt.splits.append(sp2);
                if (sp3) {
                    lt.splits.append(sp3);
                }
            }
        }
        lt.laptime = laptime;
        lt.username = QByteArray::fromStdString("/" + username);

        mDb[track][car] = lt;
    }
    mLog() << "WR database updated";
    emit updated();
    mLastUpdateTime = QDateTime::currentDateTime();
    writeFile();

    pReply->deleteLater();
}

void WrDb::loadFile () {
    QFile file("WRs.dat");
    const QString abs_path = QFileInfo(file.fileName()).absoluteFilePath();

    if (file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);

        QByteArray file_magic_string;
        qint32 actual_file_format_version;
        in >> file_magic_string;
        in >> actual_file_format_version;

        if (file_magic_string != "LfsTopWrsDb") {
            mLog(Log::WARNING) << "wrong file format: " << abs_path;
            mLog(Log::WARNING) << "moving file to: " << abs_path << ".invalid";
            if (!file.rename("Wrs.dat.invalid")) {
                mLog(Log::ERROR) << "failed to rename file: " << abs_path;
                throw std::runtime_error("failed to rename file");
            }
        } else if (actual_file_format_version != mDbFileFormatVersion) {
            // in future versions we gotta import the old file
            mLog(Log::WARNING) << "wrong file format: " << abs_path;
            mLog(Log::WARNING) << "moving file to: " << abs_path << ".invalid";
            if (!file.rename("WRs.dat.invalid")) {
                mLog(Log::ERROR) << "failed to rename file: " << abs_path;
                throw std::runtime_error("failed to rename file");
            }
        } else {
            in >> mLastUpdateTime >> mDb;
            mLog() << "WR cache loaded";
        }
    }
}

void WrDb::writeFile () {
    if (mDb.size()) {
        QFile file("WRs.dat");
        if (file.open(QIODevice::WriteOnly)) {
            QDataStream out(&file);
            out << QByteArray("LfsTopWrsDb") << mDbFileFormatVersion
                << mLastUpdateTime << mDb;
        }
        else {
            QFileInfo file_info(file.fileName());

            mLog(Log::ERROR) << "Can not write WR cache file. "
                             << "Make sure that we have write access to "
                             << file_info.absolutePath();
        }
    }
}

const QMap<QByteArray, WrDb::TrackWrs> WrDb::getDb () const {
    return mDb;
}


WrDb::WrDb (Log &pLog, const std::string &pPubstatKey)
    : mLog (pLog),
      mPubstatKey (pPubstatKey),
      mUpdateTimer (this),
      mUpdateIntervalMins (60) {
    mManager = new QNetworkAccessManager(this);
    connect(&mUpdateTimer, SIGNAL(timeout()), this, SLOT(update()));
    connect(mManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(processReply(QNetworkReply*)));
}

WrDb::~WrDb () {
    writeFile();
}

void WrDb::setUpdateIntervalMins (const int pMins) {
    mUpdateIntervalMins = pMins;
}

void WrDb::init () {
    loadFile();
    const qint64 last_update_happened =
            mLastUpdateTime.secsTo(QDateTime::currentDateTime());
    if (mLastUpdateTime.isNull() ||
            last_update_happened
            > mUpdateIntervalMins * 60) {
        mLog() << "WRs update needed";
        update();
    }
    else if (last_update_happened < 0) {
        mLog() << "WR cache file timestamp is in future, update needed";
        update();
    }
    else {
        const qint64 need_to_update_in =
                mUpdateIntervalMins * 60 - last_update_happened;
        mLog() << "WR cache is up to date";
        mUpdateTimer.setInterval(need_to_update_in * 1000);
        mUpdateTimer.start();
    }
}

void WrDb::update() {
    if (mPubstatKey.size()) {
        mUpdateTimer.setInterval(mUpdateIntervalMins * 60 * 1000);
        mUpdateTimer.start();

        mManager->get(QNetworkRequest(QUrl("http://www.lfsworld.net/pubstat"
                                          "/get_stat2.php?version=1.5&action=wr"
                                          "&idk="
                                          + QByteArray::fromStdString(mPubstatKey))));
    }
    else {
        mLog(Log::WARNING) << "Not updating WRs, pubstat key is not set";
    }
}
