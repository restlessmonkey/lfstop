// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_WR_DB_HPP_INCLUDED
#define LFSTOP_WR_DB_HPP_INCLUDED


#include <string>


#include <QObject>
#include <QMap>
#include <QDateTime>
#include <QByteArray>
#include <QTimer>


#include "Databases/LapsDbStorage.hpp"
#include "Logging/Log.hpp"
#include "Logging/LogFile.hpp"


class QNetworkAccessManager;
class QNetworkReply;


class WrDb : public QObject {
    Q_OBJECT
public:
    WrDb (Log &pLog, const std::string &pPubstatKey);
    ~WrDb ();

    void setUpdateIntervalMins (const int pMins);

    void init ();

    typedef QMap<CarT, LapsDbStorage::Laptime> TrackWrs;

    const QMap<QByteArray, TrackWrs> getDb () const;

public slots:
    void processReply (QNetworkReply *pReply);

signals:
    void updated ();

private slots:
    void update ();

private:
    void loadFile ();
    void writeFile ();

    Log &mLog;

    QNetworkAccessManager *mManager;

    QMap<QByteArray, TrackWrs> mDb;
    QDateTime mLastUpdateTime;

    const std::string mPubstatKey;

    QTimer mUpdateTimer;
    int mUpdateIntervalMins;

    static constexpr qint32 mDbFileFormatVersion = 1;
};

#endif // LFSTOP_WR_DB_HPP_INCLUDED
