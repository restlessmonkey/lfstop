// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_CONNECTION_INFO_HPP_INCLUDED
#define LFSTOP_CONNECTION_INFO_HPP_INCLUDED

#include <utility>
#include <memory>


#include <QByteArray>


#include "DataProviders/PbDataProvider.hpp"
#include "DataProviders/StintDataProvider.hpp"
#include "DataProviders/TopDataProvider.hpp"
#include "StintInfo.hpp"
#include "Databases/LapsDb.hpp"
#include "InsimWindowManager.hpp"
#include "UserSettingsInsimWindow.hpp"
#include "SwitchableInsimWindow.hpp"
#include "DataProviders/PlayersDataProvider.hpp"
#include "Utils/OverwritableLimitedContainer.hpp"
#include "TrackVoteDataProvider.hpp"
#include "Translation.hpp"
#include "DataProviders/RecentStintsDataProvider.hpp"


struct ConnectionInfo {
    class ConnectionQualityData {
    public:
        ConnectionQualityData ()
            : startTime (QDateTime::currentDateTime()) {
        }

        QDateTime startTime;
        int onlineQuantity = 0;
        int laggingQuantity = 0;
    };

    ConnectionInfo ()
        : mPbDataProvider (nullptr, "", nullptr, nullptr),
          mTopDataProvider (nullptr, nullptr, nullptr, nullptr, nullptr),
          recentStintsDataProvider (nullptr, nullptr, nullptr, nullptr, 0),
          window (nullptr, 0, false, "", "", nullptr),
          settingsWindow (nullptr, nullptr, 0, nullptr, nullptr),
          connectionQualityData (2),
          id (0) {
        throw std::runtime_error("ConnectionInfo() error");
    }

    ConnectionInfo (const std::shared_ptr<LapsDb> pDb,
                    const UsersDb * const pUsersDb,
                    LfsTcpSocket * const pSock,
                    QByteArray pUsername,
                    QByteArray pNickname,
                    const Translation::Translation &pTr,
                    const bool &pDataExportEnable,
                    const std::string &pDataExportPath,
                    const std::string &pDataExportUrlPrefix,
                    const quint8 pUcid,
                    StintInfoList * const pStints,
                    LfsServer *pLfsServer)
        : tr (new Translation::TranslationBinder(pTr, "?")),
          userSettings (new UserSettings(tr.get())),
          nickname (pNickname),
          username (pUsername),
          mPbDataProvider (pDb, pUsername, &userSettings->showSplits, tr.get()),
          mTopDataProvider (pDb,
                            pUsersDb,
                            &userSettings->showSplits,
                            &userSettings->queryClean,
                            tr.get()),
          recentStintsDataProvider (pUsersDb,
                                    pDb,
                                    pStints,
                                    pLfsServer,
                                    pUcid),
          window (pSock,
                  pUcid,
                  pDataExportEnable,
                  pDataExportPath,
                  pDataExportUrlPrefix,
                  tr.get()),
          settingsWindow (pSock, userSettings.get(), pUcid, &pTr, tr.get()),
          connectionQualityData (2),
          id (pUcid)
    {
        tb.reserve(2);
        tb.push_back(std::vector<LaptimeT>());
        tb.push_back(std::vector<LaptimeT>());
        tb[0].reserve(4);
        tb[1].reserve(4);
    }

    // these 2 we can not keep as normal members, since we have pointers to them
    // in some child members
    std::unique_ptr<Translation::TranslationBinder> tr;
    std::unique_ptr<UserSettings> userSettings;

    LapsdbQuery query;

    QByteArray nickname;
    QByteArray username;

    StintInfo stintInfo;
    LaptimeT lastSplitTime = 0;
    StintInfo::Lap currentLap;

    PbDataProvider mPbDataProvider; // switchable
    TopDataProvider mTopDataProvider; // switchable
    StintDataProvider stintDataProvider; // switchable
    RecentStintsDataProvider recentStintsDataProvider; // interactive
    SwitchableInsimWindow window;
    UserSettingsInsimWindow settingsWindow;

    TrackVoteDataProvider trackVoteDataProvider; // interactive
    InsimWindowManager windowManager;

    std::vector<std::vector<LaptimeT>> tb; // stores 2 vectors, 0th for nonclean
                                           // and 1th for clean
                                           // sections stored

    OverwritableLimitedContainer<ConnectionQualityData> connectionQualityData;

    QByteArray countryName;

    size_t currentLapNum = 0;

    TyreT currentTyres;

    CarT lastDrivenCar = CarT::unknownCar();
    CarT lastQueriedCar;

    CarT currentCar;

    quint8 previousUcid = 0;
    quint8 id;
    quint8 currentVote = VOTE_NONE;
};

#endif // LFSTOP_CONNECTION_INFO_HPP_INCLUDED
