// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_TRANSLATION_HPP_INCLUDED
#define LFSTOP_TRANSLATION_HPP_INCLUDED

#include <map>
#include <vector>
#include <string>
#include <cctype>
#include <fstream>
#include <unordered_map>

#include <QByteArray>
#include <QDir>

#include "LfsInsimUtils.hpp"

#include "Utils/StdVectorUtils.hpp"
#include "Utils/StringUtils.hpp"
#include "Utils/IostreamUtils.hpp"
#include "Logging/Log.hpp"

namespace std {
template <>
struct hash<QByteArray> {
    std::size_t operator() (const QByteArray& k) const {
        return qHash(k);
    }
  };

}

namespace Translation {

inline bool is_alphanumeric_txt_file (const QByteArray &pArr) {
    if (pArr.endsWith(".txt")) {
        for (int i = 0; i < pArr.size() - 4; ++i) {
            if (!std::isalnum(pArr[i]) && (pArr[i] != '_')) {
                return false;
            }
        }
        return true;
    }

    return false;
}

class StringIds {
public:
    enum Ids : size_t {
            CAR,
            CLASS,
            TYRES,
            USERNAME,
            SERVER_BEST_LAPS,
            BEST_POSSIBLE_LAPS_EVER,
            STINT_BEST_POSSIBLE_LAPS,
            BEST_AVERAGE_OF_3_LAPS,
            NAME,
            PERSONAL_BESTS,
            DRIVER_STINT_INFO,
            GET_TRACK_INFO,
            SHOW_CUSTOM_CARS_AND_CLASSES_DEFINED,
            SET_YOUR_PERSONAL_SETTINGS,
            VERSION_INFO_AND_SOME_STATS,
            SET_CARS_AVAILABLE_TO_DRIVE,
            SERVER_RECENT_DRIVERS,
            SHOW_RECENT_CHAT_MESSAGES,
            SHUT_DOWN_LFSTOP,
            ADMIN_COMMANDS,
            COMMANDS,
            LAPS,
            WITH_SOME_TYRES,
            QUERY_PARAMETERS_IGNORED,
            NO_DATA_FOR_REQUEST,
            NOT_FOUND,
            NO_DATA_FOR_PB_REQUEST,
            INCORRECT_REQUEST_FORMAT,
            BEST_LAP,
            BEST_STINT_LAP_POSSIBLE,
            BEST_LAP_POSSIBLE_EVER,
            SPLITS,
            SECTIONS,
            FROM_ALL,
            FROM_THIS_TYRES,
            CHAT_LOGS,
            DEFINED_CARS_AND_CLASSES,
            RECENT_PLAYERS,
            GIT_REVISION,
            SERVER_LOCAL_TIME,
            SERVERS,
            SERVER_CONNECTION_UPTIME,
            SERVER_BYTES_READ_WRITTEN,
            PROCESS_MEMORY_USAGE_KIB,
            INSTANCE_BYTES_READ_WRITTEN,
            DAYS,
            HOURS,
            MINUTES,
            SECONDS,
            ON,
            OFF,
            OK,
            ON_TRACK,
            SELF,
            OTHERS,
            OFF_TRACK,
            HLVC_CHECK_INFO,
            DISCONNECTED,
            TIMED_OUT,
            LOST_CONNECTION,
            KICKED,
            BANNED,
            SECURITY,
            CHEAT_PROTECTION,
            OUT_OF_SYNC,
            INITIAL_SYNC_FAILED,
            INVALID_PACKET,
            CONNECTED,
            AGO,
            LAP,
            CLEAN_TB,
            CLEAN_TOTAL,
            CLEAN_AVERAGE,
            TOTAL,
            AVERAGE,
            STINT_BEST_THEORETICAL_LAP,
            STINT_INFO,
            HAVENT_DONE_ANY_LAPS_YET,
            DIDNT_FIND_SUCH_PLAYER,
            EXCLUDE_REASON_NONE,
            EXCLUDE_REASON_INVALID,
            EXCLUDE_REASON_GROUND,
            EXCLUDE_REASON_WALL,
            EXCLUDE_REASON_SPEEDING,
            EXCLUDE_REASON_CONTACT,
            EXCLUDE_REASON_OBJECT_HIT,
            EXCLUDE_REASON_PITLANE,
            EXCLUDE_REASON_CAR_RESET,
            EXCLUDE_REASON_DRAFT,
            NEW_LAP_BY,
            CUSTOM_CAR,
            HLVC_VIOLATION,
            SHUTDOWN,
            QUIET,
            NOTQUIET,
            DISABLE_TRACTION_CONTROL,
            SET_CLUTCH_TO,
            AUTO,
            AXIS,
            BUTTON,
            AUTOMATIC_GEAR_SHIFTING_FORBIDDEN,
            CLEAN,
            ALL,
            PLAYERS_INFO,
            SELECT_ONE_OF,
            TYPE_COMMAND_FOR_HELP,
            EXCLUDE_REASON_OUT_OF_BOUNDS,
            WAIT_FOR_TRACK_VOTE,
            VOTE_FOR_AUTODROME,
            VOTE_FOR_TRACK,
            VOTING_CANCELLED,
            VOTED_FOR,
            AVAILABLE_CARS,
            LANGUAGE_CODE, // Added for import script convenience
            CODE_PAGE, // Added for import script convenience
            LAPSDB_QUERY_TOP_LAPS_HEADER,
            LAPSDB_QUERY_TBS_LAPS_HEADER,
            LAPSDB_QUERY_TB_LAPS_HEADER,
            LAPSDB_QUERY_AVG_LAPS_HEADER,
            LANGUAGE_NATIVE_NAME,
            WINDOW_DATA_EXPORT,
            WINDOW_NEXT_PAGE,
            WINDOW_PREVIOUS_PAGE,
            RECENT_STINTS,
            NUM_LAPS,
            VOTE_CANCELLED,
            VOTE_UNANIMOUS_REQUIRED,
            _ILLEGAL_ELEMENT_,
    };

    StringIds ()
        : mStrings ({
                   "CAR",
                   "CLASS",
                   "TYRES",
                   "USERNAME",
                   "SERVER_BEST_LAPS",
                   "BEST_POSSIBLE_LAPS_EVER",
                   "STINT_BEST_POSSIBLE_LAPS",
                   "BEST_AVERAGE_OF_3_LAPS",
                   "NAME",
                   "PERSONAL_BESTS",
                   "DRIVER_STINT_INFO",
                   "GET_TRACK_INFO",
                   "SHOW_CUSTOM_CARS_AND_CLASSES_DEFINED",
                   "SET_YOUR_PERSONAL_SETTINGS",
                   "VERSION_INFO_AND_SOME_STATS",
                   "SET_CARS_AVAILABLE_TO_DRIVE",
                   "SERVER_RECENT_DRIVERS",
                   "SHOW_RECENT_CHAT_MESSAGES",
                   "SHUT_DOWN_LFSTOP",
                   "ADMIN_COMMANDS",
                   "COMMANDS",
                   "LAPS",
                   "WITH_SOME_TYRES",
                   "QUERY_PARAMETERS_IGNORED",
                   "NO_DATA_FOR_REQUEST",
                   "NOT_FOUND",
                   "NO_DATA_FOR_PB_REQUEST",
                   "INCORRECT_REQUEST_FORMAT",
                   "BEST_LAP",
                   "BEST_STINT_LAP_POSSIBLE",
                   "BEST_LAP_POSSIBLE_EVER",
                   "SPLITS",
                   "SECTIONS",
                   "FROM_ALL",
                   "FROM_THIS_TYRES",
                   "CHAT_LOGS",
                   "DEFINED_CARS_AND_CLASSES",
                   "RECENT_PLAYERS",
                   "GIT_REVISION",
                   "SERVER_LOCAL_TIME",
                   "SERVERS",
                   "SERVER_CONNECTION_UPTIME",
                   "SERVER_BYTES_READ_WRITTEN",
                   "PROCESS_MEMORY_USAGE_KIB",
                   "INSTANCE_BYTES_READ_WRITTEN",
                   "DAYS",
                   "HOURS",
                   "MINUTES",
                   "SECONDS",
                   "ON",
                   "OFF",
                   "OK",
                   "ON_TRACK",
                   "SELF",
                   "OTHERS",
                   "OFF_TRACK",
                   "HLVC_CHECK_INFO",
                   "DISCONNECTED",
                   "TIMED_OUT",
                   "LOST_CONNECTION",
                   "KICKED",
                   "BANNED",
                   "SECURITY",
                   "CHEAT_PROTECTION",
                   "OUT_OF_SYNC",
                   "INITIAL_SYNC_FAILED",
                   "INVALID_PACKET",
                   "CONNECTED",
                   "AGO",
                   "LAP",
                   "CLEAN_TB",
                   "CLEAN_TOTAL",
                   "CLEAN_AVERAGE",
                   "TOTAL",
                   "AVERAGE",
                   "STINT_BEST_THEORETICAL_LAP",
                   "STINT_INFO",
                   "HAVENT_DONE_ANY_LAPS_YET",
                   "DIDNT_FIND_SUCH_PLAYER",
                   "EXCLUDE_REASON_NONE",
                   "EXCLUDE_REASON_INVALID",
                   "EXCLUDE_REASON_GROUND",
                   "EXCLUDE_REASON_WALL",
                   "EXCLUDE_REASON_SPEEDING",
                   "EXCLUDE_REASON_CONTACT",
                   "EXCLUDE_REASON_OBJECT_HIT",
                   "EXCLUDE_REASON_PITLANE",
                   "EXCLUDE_REASON_CAR_RESET",
                   "EXCLUDE_REASON_DRAFT",
                   "NEW_LAP_BY",
                   "CUSTOM_CAR",
                   "HLVC_VIOLATION",
                   "SHUTDOWN",
                   "QUIET",
                   "NOTQUIET",
                   "DISABLE_TRACTION_CONTROL",
                   "SET_CLUTCH_TO",
                   "AUTO",
                   "AXIS",
                   "BUTTON",
                   "AUTOMATIC_GEAR_SHIFTING_FORBIDDEN",
                   "CLEAN",
                   "ALL",
                   "PLAYERS_INFO",
                   "SELECT_ONE_OF",
                   "TYPE_COMMAND_FOR_HELP",
                   "EXCLUDE_REASON_OUT_OF_BOUNDS",
                   "WAIT_FOR_TRACK_VOTE",
                   "VOTE_FOR_AUTODROME",
                   "VOTE_FOR_TRACK",
                   "VOTING_CANCELLED",
                   "VOTED_FOR",
                   "AVAILABLE_CARS",
                   "LANGUAGE_CODE",
                   "CODE_PAGE",
                   "LAPSDB_QUERY_TOP_LAPS_HEADER",
                   "LAPSDB_QUERY_TBS_LAPS_HEADER",
                   "LAPSDB_QUERY_TB_LAPS_HEADER",
                   "LAPSDB_QUERY_AVG_LAPS_HEADER",
                   "LANGUAGE_NATIVE_NAME",
                   "WINDOW_DATA_EXPORT",
                   "WINDOW_NEXT_PAGE",
                   "WINDOW_PREVIOUS_PAGE",
                   "RECENT_STINTS",
                   "NUM_LAPS",
                   "VOTE_CANCELLED",
                   "VOTE_UNANIMOUS_REQUIRED",
              }) {
    }

    std::string toString (const Ids id) {
        if (id < mStrings.size()) {
            return mStrings[id];
        }
        return "___ERROR___";
    }

    Ids fromString (const std::string &pString) {
        auto it = std::find(mStrings.begin(), mStrings.end(), pString);
        if (it != mStrings.end()) {
            return (Ids) std::distance(mStrings.begin(), it);
        }
        return Ids::_ILLEGAL_ELEMENT_;
    }

private:
    std::vector<std::string> mStrings;
};

inline char get_encoding_symbol (const std::string &pStr) {
    if (pStr == "1252") {
        return 'L';
    } else if (pStr == "1251") {
        return 'C';
    } else if (pStr == "1253") {
        return 'G';
    } else if (pStr == "932") {
        return 'J';
    } else if (pStr == "1250") {
        return 'E';
    } else if (pStr == "1254") {
        return 'T';
    } else if (pStr == "1257") {
        return 'B';
    } else if (pStr == "950") {
        return 'H';
    } else if (pStr == "936") {
        return 'S';
    } else if (pStr == "949") {
        return 'K';
    }
    return 'L';
}

inline std::vector<QByteArray> validate_translation_string (const QByteArray &pEng,
                                                            const QByteArray &pTr) {
    StringUtils::QByteArrayTokenizer tok (pEng, " :");
    std::vector<QByteArray> subs;
    std::vector<QByteArray> not_found_subs;
    for (const auto &el : tok.getResultList()) {
        if (el[0] == '%') {
            subs.push_back(el);
        }
    }

    for (const auto &el : subs) {
        if (!pTr.contains(el)) {
            not_found_subs.push_back(el);
        }
    }

    return not_found_subs;
}

inline QByteArray getLanguageName (const QByteArray &pLangCode) {
    static const std::unordered_map<QByteArray, QByteArray> langs {
        { "ENG", "English" },
        { "DEU", "German" },
        { "POR", "Portuguese" },
        { "FRE", "French" },
        { "FIN", "Suomi" },
        { "NOR", "Norsk" },
        { "DUT", "Nederlands" },
        { "CAT", "Catalan" },
        { "TUR", "Turkish" },
        { "SPA", "Castellano" },
        { "ITA", "Italiano" },
        { "DAN", "Dansk" },
        { "CZE", "Czech" },
        { "RUS", "Russian" },
        { "EST", "Estonian" },
        { "SRP", "Serbian" },
        { "GRE", "Greek" },
        { "POL", "Polski" },
        { "HRV", "Croatian" },
        { "HUN", "Hungarian" },
        { "BRA", "Brazilian" },
        { "SWE", "Swedish" },
        { "SLO", "Slovak" },
        { "GLG", "Galego" },
        { "SLV", "Slovenski" },
        { "BEL", "Belarusian" },
        { "LAV", "Latvian" },
        { "LIT", "Lithuanian" },
        { "CHL", "Trad. Chinese" },
        { "CHI", "Chinese" },
        { "JPN", "Japanese" },
        { "KOR", "Korean" },
        { "BUL", "Bulgarian" },
        { "LAT", "Latino" },
        { "UKR", "Ukrainian" },
        { "IND", "Indonesian" },
        { "RUM", "Romanian" }
    };
    if (langs.find(pLangCode) != std::end(langs)) {
        return langs.at(pLangCode);
    }
    return "";
}

class Translation {
public:
    Translation () {
        mData["ENG"] =
            std::map<StringIds::Ids, QByteArray> {
              { StringIds::CAR,
                QByteArray("car") },
              { StringIds::CLASS,
                "class" },
              { StringIds::TYRES,
                "tyres" },
              { StringIds::USERNAME,
                "username" },
              { StringIds::SERVER_BEST_LAPS,
                "server best laps" },
              { StringIds::BEST_POSSIBLE_LAPS_EVER,
                "best possible laps ever" },
              { StringIds::STINT_BEST_POSSIBLE_LAPS,
                "stint best possible lap" },
              { StringIds::BEST_AVERAGE_OF_3_LAPS,
                "best average of 3 laps" },
              { StringIds::NAME,
                "name" },
              { StringIds::PERSONAL_BESTS,
                "personal bests" },
              { StringIds::DRIVER_STINT_INFO,
                "driver's stint info" },
              { StringIds::GET_TRACK_INFO,
                "get current track info" },
              { StringIds::SHOW_CUSTOM_CARS_AND_CLASSES_DEFINED,
                "show custom cars and classes defined" },
              { StringIds::SET_YOUR_PERSONAL_SETTINGS,
                "set your personal settings" },
              { StringIds::VERSION_INFO_AND_SOME_STATS,
                "version info and some stats" },
              { StringIds::SET_CARS_AVAILABLE_TO_DRIVE,
                "set cars available to drive" },
              { StringIds::SERVER_RECENT_DRIVERS,
                "server recent drivers" },
              { StringIds::SHOW_RECENT_CHAT_MESSAGES,
                "show recent chat messages" },
              { StringIds::SHUT_DOWN_LFSTOP,
                "shut down LFSTop" },
              { StringIds::ADMIN_COMMANDS,
                "Admin commands" },
              { StringIds::COMMANDS,
                "commands" },
              { StringIds::LAPS,
                "laps" },
              { StringIds::WITH_SOME_TYRES,
                "with %s tyres" },
              { StringIds::QUERY_PARAMETERS_IGNORED,
                "Query parameters ignored: %s" },
              { StringIds::NO_DATA_FOR_REQUEST,
                "No %table_type data for request: %car_track_tyres" },
              { StringIds::NOT_FOUND,
                "Not found: %s" },
              { StringIds::NO_DATA_FOR_PB_REQUEST,
                "No data for driver %name: %car_track_tyres" },
              { StringIds::INCORRECT_REQUEST_FORMAT,
                "Incorrect request format" },
              { StringIds::BEST_LAP,
                "Best lap" },
              { StringIds::BEST_STINT_LAP_POSSIBLE,
                "Best stint lap possible" },
              { StringIds::BEST_LAP_POSSIBLE_EVER,
                "Best lap possible ever" },
              { StringIds::SPLITS,
                "splits" },
              { StringIds::SECTIONS,
                "sections" },
              { StringIds::FROM_ALL,
                "from all" },
              { StringIds::FROM_THIS_TYRES,
                "on these tyres" },
              { StringIds::CHAT_LOGS,
                "chat logs" },
              { StringIds::DEFINED_CARS_AND_CLASSES,
                "defined cars and classes" },
              { StringIds::RECENT_PLAYERS,
                "recent players" },
              { StringIds::GIT_REVISION,
                "git revision" },
              { StringIds::SERVER_LOCAL_TIME,
                "Server local time: %s" },
              { StringIds::SERVERS,
                "Servers: %s" },
              { StringIds::SERVER_CONNECTION_UPTIME,
                "Server connection uptime: %s" },
              { StringIds::SERVER_BYTES_READ_WRITTEN,
                "Server bytes read/written: %s" },
              { StringIds::PROCESS_MEMORY_USAGE_KIB,
                "Process memory usage (KiB): %s" },
              { StringIds::INSTANCE_BYTES_READ_WRITTEN,
                "Instance bytes read/written: %s" },
              { StringIds::DAYS,
                "days" },
              { StringIds::HOURS,
                "hours" },
              { StringIds::MINUTES,
                "minutes" },
              { StringIds::SECONDS,
                "seconds" },
              { StringIds::ON,
                "On" },
              { StringIds::OFF,
                "Off" },
              { StringIds::OK,
                "Ok" },
              { StringIds::ON_TRACK,
                "On track" },
              { StringIds::SELF,
                "Self" },
              { StringIds::OTHERS,
                "Others" },
              { StringIds::OFF_TRACK,
                "Off track" },
              { StringIds::HLVC_CHECK_INFO,
                "HLVC check info" },
              { StringIds::DISCONNECTED,
                "disconnected" },
              { StringIds::TIMED_OUT,
                "timed out" },
              { StringIds::LOST_CONNECTION,
                "lost connection" },
              { StringIds::KICKED,
                "kicked" },
              { StringIds::BANNED,
                "banned" },
              { StringIds::SECURITY,
                "security" },
              { StringIds::CHEAT_PROTECTION,
                "cheat protection wrong" },
              { StringIds::OUT_OF_SYNC,
                "out of sync" },
              { StringIds::INITIAL_SYNC_FAILED,
                "initial sync failed" },
              { StringIds::INVALID_PACKET,
                "invalid packet" },
              { StringIds::CONNECTED,
                "connected" },
              { StringIds::AGO,
                "%s ago" },
              { StringIds::LAP,
                "Lap %s" },
              { StringIds::CLEAN_TB,
                "Clean TB" },
              { StringIds::CLEAN_TOTAL,
                "Clean total" },
              { StringIds::CLEAN_AVERAGE,
                "Clean average" },
              { StringIds::TOTAL,
                "Total" },
              { StringIds::AVERAGE,
                "Average" },
              { StringIds::STINT_INFO,
                "%s stint info" },
              { StringIds::HAVENT_DONE_ANY_LAPS_YET,
                "%s haven't done any laps yet" },
              { StringIds::DIDNT_FIND_SUCH_PLAYER,
                "Didn't find such player" },
              { StringIds::EXCLUDE_REASON_NONE,
                "NONE" },
              { StringIds::EXCLUDE_REASON_INVALID,
                "INVALID" },
              { StringIds::EXCLUDE_REASON_GROUND,
                "GROUND" },
              { StringIds::EXCLUDE_REASON_WALL,
                "WALL" },
              { StringIds::EXCLUDE_REASON_SPEEDING,
                "SPEEDING" },
              { StringIds::EXCLUDE_REASON_CONTACT,
                "CONTACT" },
              { StringIds::EXCLUDE_REASON_OBJECT_HIT,
                "OBJECT_HIT" },
              { StringIds::EXCLUDE_REASON_PITLANE,
                "PITLANE" },
              { StringIds::EXCLUDE_REASON_CAR_RESET,
                "CAR_RESET" },
              { StringIds::EXCLUDE_REASON_DRAFT,
                "DRAFT" },
              { StringIds::NEW_LAP_BY,
                "New %car %type by %player: %laptime" },
              { StringIds::CUSTOM_CAR,
                "%driver custom car: %car" },
              { StringIds::HLVC_VIOLATION,
                "HLVC violation: %s" },
              { StringIds::SHUTDOWN,
                "shutdown" },
              { StringIds::QUIET,
                "quiet mode" },
              { StringIds::NOTQUIET,
                "quiet mode off (get back to normal)" },
              { StringIds::DISABLE_TRACTION_CONTROL,
                "disable traction control in car setup" },
              { StringIds::SET_CLUTCH_TO,
                "set clutch to one of: %s" },
              { StringIds::AUTO,
                "auto" },
              { StringIds::AXIS,
                "axis" },
              { StringIds::BUTTON,
                "button" },
              { StringIds::AUTOMATIC_GEAR_SHIFTING_FORBIDDEN,
                "automatic gear shifting forbidden" },

              { StringIds::CLEAN,
                "clean" },
              { StringIds::ALL,
                "all" },

              { StringIds::PLAYERS_INFO,
                "players info" },
              { StringIds::SELECT_ONE_OF,
                "Select one of: %s" },
              { StringIds::TYPE_COMMAND_FOR_HELP,
                "Type %s for help" },
              { StringIds::EXCLUDE_REASON_OUT_OF_BOUNDS,
                "OUT OF BOUNDS" },
              { StringIds::WAIT_FOR_TRACK_VOTE,
                "Wait for vote or track loading to finish" },
              { StringIds::VOTE_FOR_AUTODROME,
                "Vote for autodrome" },
              { StringIds::VOTE_FOR_TRACK,
                "Vote for track" },
              { StringIds::VOTING_CANCELLED,
                "Vote cancelled" },
              { StringIds::VOTED_FOR,
                "Voted for %s" },
              { StringIds::AVAILABLE_CARS,
                "Available cars: %s" },

              { StringIds::LANGUAGE_CODE,
                "ENG" },
              { StringIds::CODE_PAGE,
                "1252" },

              { StringIds::STINT_BEST_THEORETICAL_LAP,
                "Best TB" },

              { StringIds::LAPSDB_QUERY_TOP_LAPS_HEADER,
                "Top %car_or_class laps (%total_records) at"
                " %track %with_some_tyres" },
              { StringIds::LAPSDB_QUERY_TBS_LAPS_HEADER,
                "Stint best possible %car_or_class laps "
                "(%total_records) at %track %with_some_tyres" },
              { StringIds::LAPSDB_QUERY_TB_LAPS_HEADER,
                "Theoretical %car_or_class laps "
                "(%total_records) at %track %with_some_tyres" },
              { StringIds::LAPSDB_QUERY_AVG_LAPS_HEADER,
                "Top %car_or_class average of 3 laps (%total_records) at "
                "%track %with_some_tyres" },
              { StringIds::LANGUAGE_NATIVE_NAME,
                "English" },
              { StringIds::WINDOW_DATA_EXPORT,
                "export" },
              { StringIds::WINDOW_NEXT_PAGE,
                "Next" },
              { StringIds::WINDOW_PREVIOUS_PAGE,
                "Previous" },
              { StringIds::RECENT_STINTS,
                "recent stints" },
              { StringIds::NUM_LAPS,
                "%s laps" },

              { StringIds::VOTE_CANCELLED,
                "Your vote cancelled" },
              { StringIds::VOTE_UNANIMOUS_REQUIRED,
                "Unanimous vote required, rejecting for now (repeat to cancel)" },
    };

        mLanguages = { "ENG" };

        QDir dir ("languages/");
        QFileInfoList files = dir.entryInfoList(QStringList("*.txt"),
                                                QDir::Files | QDir::Readable);

        if (files.isEmpty()) {
            return;
        }


        for (QFileInfo file: files) {
            if (is_alphanumeric_txt_file(file.fileName().toLatin1())) {
                loadTranslationFile(file.absoluteFilePath().toLatin1(),
                                    file.baseName().toLatin1());
            }
        }
    }

    QByteArray get (const StringIds::Ids pId, const QByteArray &pLang) const {
        if (mData.count(pLang) && mData.at(pLang).count(pId)) {
            return mData.at(pLang).at(pId);
        }
        return mData.at("ENG").at(pId);
    }

    QByteArray getExact (const StringIds::Ids pId,
                         const QByteArray &pLang) const {
        if (mData.count(pLang) && mData.at(pLang).count(pId)) {
            return mData.at(pLang).at(pId);
        }
        return "";
    }

    const std::vector<QByteArray> &getLanguages () const {
        return mLanguages;
    }

    void dumpTranslationData () {
        std::ofstream file ("ENG.txt");
        for (const auto &el : mData["ENG"]) {
            file << mIds.toString(el.first)
                 << " "
                 << el.second.constData() << '\n';
        }
    }

    void validateTranslations (Log &log) {
        const auto &en = mData.at("ENG");
        for (const auto &el: mData) {
            const auto &lang = el.first;
            if (lang == "ENG") {
                continue;
            }
            const auto &tr = el.second;

            for (const auto &trel : tr) {
                const auto id = trel.first;
                const auto &value = trel.second;
                const auto not_found =
                    validate_translation_string(en.at(id), value);
                if (not_found.size()) {

                    QByteArray str;
                    for (const auto &e : not_found) {
                        str += e + ' ';
                    }


                    log(Log::ERROR)
                        << lang << ' '
                        << mIds.toString(id) << ' '
                        << "not found:" << ' '
                        << str;

                }
            }
        }
    }

private:
    void loadTranslationFile (const QByteArray &pFile, const QByteArray &pLang) {
        if (!vector_contains(mLanguages, pLang)) {
            mLanguages.push_back(pLang);
        }

        QByteArray encoding = "^L";

        std::string str;

        std::ifstream file(pFile.constData());

        while(!IostreamUtils::safeGetline(file, str).eof() || !file.fail()) {
            str = StringUtils::trim(str, " \t");

            if (str.length() > 0 && str.at(0) != '#') {
                std::string::size_type separator_pos =
                        str.find_first_of(" \n\0", 0);

                std::string name = str.substr(0, separator_pos);
                std::string tr =
                        (separator_pos + 1 == std::string::npos
                         || separator_pos == std::string::npos)
                        ? ""
                        : str.substr(separator_pos + 1);


                if (name == "CODE_PAGE") {
                    encoding[1] = get_encoding_symbol(tr);
                }
                else {
                    try {
                        const StringIds::Ids id = mIds.fromString(name.data());
                        if (id != StringIds::Ids::_ILLEGAL_ELEMENT_) {
                            mData[pLang][id] =
                                    encoding + QByteArray::fromStdString(tr);
                        }
                    } catch (std::runtime_error &err) {
                        (void) err;
                    }
                }
            }
        }
    }

    // Lang : (Id : String);
    std::map<QByteArray, std::map<StringIds::Ids, QByteArray>> mData;
    std::vector<QByteArray> mLanguages;
    LfsInsimUtils::StringDecoder mDecoder;

    StringIds mIds;
};

class TranslationBinder {
public:
    TranslationBinder (const Translation &pTr,
                       const QByteArray &pLang)
        : mTr (pTr),
          mLang (pLang) {
    }
    QByteArray get (const StringIds::Ids pId) const {
        return mTr.get(pId, mLang);
    }
    QByteArray getExact(const StringIds::Ids pId) const {
        return mTr.getExact(pId, mLang);
    }
    void resetLang (const QByteArray& pLang) {
        mLang = pLang;
    }

private:
    const Translation &mTr;
    QByteArray mLang;
};

} // namespace Translation

#endif // LFSTOP_TRANSLATION_HPP_INCLUDED

