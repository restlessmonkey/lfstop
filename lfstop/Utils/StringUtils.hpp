// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_STRING_UTILS_HPP_INCLUDED
#define LFSTOP_STRING_UTILS_HPP_INCLUDED

#include <QByteArray>
#include <QPair>

#include <string>
#include <algorithm>
#include <sstream>
#include <vector>
#include <utility>
#include <cctype>

namespace StringUtils {
    inline std::string trim (const std::string &pString,
                             const std::string &pWhitespace = " \t") {
        const size_t beginStr = pString.find_first_not_of(pWhitespace);

        if (beginStr == std::string::npos) {
            // no content
            return "";
        }

        const size_t endStr = pString.find_last_not_of(pWhitespace);
        const size_t range = endStr - beginStr + 1;
        return pString.substr(beginStr, range);
    }

    inline std::string reduce (const std::string &pString,
                               const std::string &pFill = " ",
                               const std::string &pWhitespace = " \t") {
        // trim first
        std::string result(trim(pString, pWhitespace));
        // replace sub ranges
        size_t beginSpace = result.find_first_of(pWhitespace);

        while (beginSpace != std::string::npos) {
            const size_t endSpace =
                result.find_first_not_of(pWhitespace, beginSpace);
            const size_t range = endSpace - beginSpace;
            result.replace(beginSpace, range, pFill);
            const size_t newStart = beginSpace + pFill.length();
            beginSpace = result.find_first_of(pWhitespace, newStart);
        }

        return result;
    }


    inline std::string capitalized (const std::string &str) {
        std::string ret_str = str;

        if (ret_str.length() > 0) {
            ret_str[0] = toupper(ret_str[0]);
        }
        return ret_str;
    }

    inline void capitalize (std::string &str) {
        str[0] = toupper(str[0]);
    }


    inline std::string allcapsed (const std::string &str) {
        std::string ret_str = str;
        for (size_t i = 0; i < str.size(); ++i) {
            ret_str[i] = toupper(str[i]);
        }
        return ret_str;
    }

    inline void allcaps (std::string &str) {
        for (size_t i = 0; i < str.size(); ++i) {
            str[i] = toupper(str[i]);
        }
    }

    inline std::string alllowered (const std::string &str) {
        std::string ret_str = str;
        for (size_t i = 0; i < str.size(); ++i) {
            ret_str[i] = tolower(str[i]);
        }
        return ret_str;
    }

    inline void alllower (std::string &str) {
        for (size_t i = 0; i < str.size(); ++i) {
            str[i] = tolower(str[i]);
        }
    }


    template <typename T>
    inline T fromString (const std::string &str) {
        std::stringstream ss_val(str);
        T ret_val;
        ss_val >> ret_val;
        return ret_val;
    }

    template<>
    inline bool fromString<bool> (const std::string &str) {
        if (alllowered(str) == "true"
                || alllowered(str) == "yes"
                || str == "1") {
            return true;
        }
        return false;
    }

    template <typename T>
    inline std::string toString (const T &val) {
        std::stringstream ss_val;
        ss_val << val;
        return ss_val.str();
    }

    inline std::vector<std::string> stringToVector (const std::string &pStr) {
        std::vector<std::string> ret;

        std::string word;

        for (std::stringstream s(pStr); s >> word; ) {
            ret.push_back(word);
        }
        return ret;
    }

    inline std::vector<QByteArray>
    stringToQByteArrayVector (const std::string &pStr) {
        std::vector<QByteArray> ret;

        std::string word;

        for (std::stringstream s(pStr); s >> word; ) {
            ret.push_back(QByteArray::fromStdString(word));
        }
        return ret;
    }

    inline std::string padRight (const std::string &pStr,
                                 const size_t pPadTo,
                                 const bool pTruncate = false) {
        const size_t len_orig = pStr.length();
        if (len_orig < pPadTo) {
            return pStr + std::string(pPadTo - len_orig, ' ');
        }
        else if (pTruncate && len_orig > pPadTo) {
            return pStr.substr(0, pPadTo);
        }
        return pStr;
    }

    inline bool isUnsignedInteger (const std::string &pStr) {
        std::string::const_iterator it = pStr.begin();
        while (it != pStr.end() && std::isdigit(*it)) {
            ++it;
        }
        return !pStr.empty() && it == pStr.end();
    }

    inline std::string unquoted (const std::string &pStr) {
        // warning: assuming that string starts with quote!
        size_t second_quote_pos = pStr.find_first_of('"', 1);
        return pStr.substr(1, second_quote_pos - 1);
    }

    inline QByteArray unquoted (const QByteArray &pStr) {
        // warning: assuming that string starts with quote!
        int second_quote_pos = pStr.indexOf('"', 1);
        return pStr.mid(1, second_quote_pos - 1);
    }

    inline int QByteArrayFindFirstNotOf (const QByteArray &pStr,
                                         const char c,
                                         const int pos = 0) {
        for (int i = pos; i < pStr.size(); ++i) {
            if (pStr[i] != c) {
                return i;
            }
        }
        return -1;
    }

    inline int QByteArrayFindFirstNotOf (const QByteArray &pStr,
                                         const QByteArray &pSeps,
                                         const int pos = 0) {
        for (int i = pos; i < pStr.size(); ++i) {
            if (!pSeps.contains(pStr[i])) {
                return i;
            }
        }
        return -1;
    }

    inline int QByteArrayFindFirstOf (const QByteArray &pStr,
                                      const QByteArray &pSeps,
                                      const int pos = 0) {
        for (int i = pos; i < pStr.size(); ++i) {
            if (pSeps.contains(pStr[i])) {
                return i;
            }
        }
        return -1;
    }

    class QByteArrayTokenizer {
    public:
        QByteArrayTokenizer (const QByteArray &pStr)
            : mString (pStr) {
            int pos = 0;
            while (pos != -1) {
                const int word_start_pos =
                        QByteArrayFindFirstNotOf(mString, ' ', pos);
                if (word_start_pos == -1) {
                    break;
                }

                if (mString[word_start_pos] == '"') {
                    pos = mString.indexOf('"', word_start_pos + 1);
                    const int length =
                            (pos == -1)
                            ? mString.size() - word_start_pos
                            : pos - word_start_pos - 1;
                    mWords.push_back({word_start_pos + 1, length});

                    ++pos;
                } else {
                    pos = mString.indexOf(' ', word_start_pos);
                    const int length =
                            (pos == -1)
                            ? mString.size() - word_start_pos
                            : pos - word_start_pos;
                    mWords.push_back({word_start_pos, length});
                }
            }
        }
        QByteArrayTokenizer (const QByteArray &pStr,
                             const QByteArray &pSeps)
            : mString (pStr) {
            int pos = 0;
            while (pos != -1) {
                const int word_start_pos =
                        QByteArrayFindFirstNotOf(mString, pSeps, pos);
                if (word_start_pos == -1) {
                    break;
                }

                if (mString[word_start_pos] == '"') {
                    pos = mString.indexOf('"', word_start_pos + 1);
                    const int length =
                            (pos == -1)
                            ? mString.size() - word_start_pos
                            : pos - word_start_pos - 1;
                    mWords.push_back({word_start_pos + 1, length});

                    ++pos;
                } else {
                    pos = QByteArrayFindFirstOf(mString, pSeps, word_start_pos);
                    const int length =
                            (pos == -1)
                            ? mString.size() - word_start_pos
                            : pos - word_start_pos;
                    mWords.push_back({word_start_pos, length});
                }
            }
        }
        QByteArray getWord (const size_t pWordNum) {
            return mString.mid(mWords[pWordNum].first,
                               mWords[pWordNum].second);
        }
        QByteArray getWords (const size_t pStartFrom,
                             const size_t pNumWords) {
            const int end_pos = mWords[pStartFrom + pNumWords - 1].first
                    + mWords[pStartFrom + pNumWords - 1].second;
            return mString.mid(mWords[pStartFrom].first,
                               end_pos - mWords[pStartFrom].first);
        }
        QByteArray getRestAt (const size_t pWordNum) {
            return mString.mid(mWords[pWordNum].first, -1);
        }
        size_t getNumWords () {
            return mWords.size();
        }

        std::vector<QByteArray> getResultList () {
            std::vector<QByteArray> ret;
            for (size_t i = 0; i < getNumWords(); ++i) {
                ret.push_back(getWord(i));
            }
            return ret;
        }

    private:
        std::vector<QPair<int, int>> mWords; // post, length
        QByteArray mString;
    };

    class Tokenizer {
    public:
        Tokenizer (const std::string &pStr)
            : mString (pStr) {
            size_t pos = 0;
            while (pos != mString.npos) {
                const size_t word_start_pos = mString.find_first_not_of(' ', pos);
                if (word_start_pos == mString.npos) {
                    break;
                }
                pos = mString.find_first_of(' ', word_start_pos);
                const size_t length =
                        (pos == mString.npos)
                        ? mString.size() - word_start_pos
                        : pos - word_start_pos;
                mWords.push_back(std::pair<size_t, size_t>(word_start_pos, length));
            }
        }
        std::string getWord (const size_t pWordNum) {
            return mString.substr(mWords[pWordNum].first,
                                  mWords[pWordNum].second);
        }
        std::string getWords (const size_t pStartFrom,
                              const size_t pNumWords) {
            const size_t end_pos = mWords[pStartFrom + pNumWords - 1].first
                    + mWords[pStartFrom + pNumWords - 1].second;
            return mString.substr(mWords[pStartFrom].first,
                                  end_pos - mWords[pStartFrom].first);
        }
        std::string getRestAt (const size_t pWordNum) {
            return mString.substr(mWords[pWordNum].first,
                                  mString.npos);
        }
        size_t getNumWords () {
            return mWords.size();
        }

    private:
        std::vector<std::pair<size_t, size_t>> mWords; // post, length
        std::string mString;
    };

    inline int QByteArrayFindFirstOfTwo (const QByteArray &pArr,
                                         const char pChar1,
                                         const char pChar2,
                                         const int pStartFrom) {
        for (int i = pStartFrom; i < pArr.size(); ++i) {
            const char c = pArr[i];
            if ((c == pChar1) || (c == pChar2)) {
                return i;
            }
        }
        return -1;
    }

    inline bool caseInsCharCompareN (const char a, const char b) {
        return(toupper(a) == toupper(b));
    }

    inline bool compareCaseInsensitive (const QByteArray &pArr1,
                                        const QByteArray &pArr2) {
        return pArr1.size() == pArr2.size()
                && std::equal(pArr1.begin(),
                              pArr1.end(),
                              pArr2.begin(),
                              caseInsCharCompareN);
    }

    template<typename IntegerT = unsigned int>
    QByteArray toBase36 (IntegerT val) {
        static QByteArray base36 = "0123456789abcdefghijklmnopqrstuvwxyz";
        QByteArray result;
        result.reserve(14);
        do {
            result = base36[static_cast<int>(val % 36)] + result;
        } while (val /= 36);
        return result;
    }
}

#endif // LFSTOP_STRING_UTILS_HPP_INCLUDED
