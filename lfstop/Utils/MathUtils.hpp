// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_MATH_UTILS_HPP_INCLUDED
#define LFSTOP_MATH_UTILS_HPP_INCLUDED

#include <cmath>

namespace MathUtils {

struct Vector2 {
    float x;
    float y;

    Vector2 operator/ (const float pNum) {
        Vector2 ret;
        ret.x = x / pNum;
        ret.y = y / pNum;
        return ret;
    }
};

inline Vector2 operator- (const Vector2 &pVec1, const Vector2 &pVec2) {
    Vector2 ret;
    ret.x = pVec1.x - pVec2.x;
    ret.y = pVec1.y - pVec2.y;
    return ret;
}

constexpr float Pi = 3.1415926f;

inline float absf (const float pVal) {
    return pVal > 0 ? pVal : -pVal;
}

inline float sqr (const float pVal) {
    return pVal * pVal;
}

inline float distance2f (const float pPoint1X, const float pPoint1Y,
                  const float pPoint2X, const float pPoint2Y) {
    return sqrt(sqr(pPoint2X - pPoint1X) + sqr(pPoint2Y - pPoint1Y));
}

inline float degreesToRadians (const float pDegrees) {
    return pDegrees * Pi / 180.f;
}

inline float radiansToDegrees (const float pRadians) {
    return pRadians * 180 / Pi;
}


inline float angleDistance2f (const float a, const float b) {
  float r1, r2;
  if (a > b) {
      r1 = a - b;
      r2 = b - a + 2 * Pi;
  } else {
      r1 = b - a;
      r2 = a - b + 2 * Pi;
  }
  return (r1 > r2) ? r2 : r1;
}

inline float kmPerHourToMetersPerSec (const float pVal) {
    return pVal * 1000.f / (60 * 60);
}

template<typename T>
inline T sign (const T pVar) {
    return pVar >= 0 ? 1 : -1;
}

template<typename T>
inline T abs (const T pVar) {
    return pVar >= 0 ? pVar : -pVar;
}

} // namespace MathUtils

#endif // LFSTOP_MATH_UTILS_HPP_INCLUDED
