// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_PROCESS_MEMORY_GETTER_HPP_INCLUDED
#define LFSTOP_PROCESS_MEMORY_GETTER_HPP_INCLUDED


#include <cstddef>


namespace ProcessMemoryGetter {
size_t getCurrentMemoryUsageInKilobytes();
}


#endif //LFSTOP_PROCESS_MEMORY_GETTER_HPP_INCLUDED
