// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "ProcessMemoryGetter.hpp"


#ifndef _WIN32
#include <unistd.h>
#include <QFile>
#else //_WIN32
#include <windows.h>
#include <psapi.h>
#endif //_WIN32


namespace ProcessMemoryGetter {
#ifndef _WIN32
size_t getCurrentMemoryUsageInKilobytes () {
    size_t page_size = (size_t) getpagesize();
    QFile f("/proc/self/statm");
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return 0;
    }
    // assuming we never gonna eat more than 4 gigabytes of memory :)
    size_t num = f.readLine().split(' ')[2].toUInt();
    return num * page_size / 1024;
}
#else //_WIN32
size_t getCurrentMemoryUsageInKilobytes () {
    PROCESS_MEMORY_COUNTERS pmc;
    bool success = GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));

    if (success) {
        return pmc.WorkingSetSize / 1024;
    }
    return 0;
}
#endif //_WIN32
} // namespace ProcessMemoryGetter
