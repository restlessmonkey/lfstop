// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_OVERWRITABLE_LIMITED_CONTAINER_HPP_INCLUDED
#define LFSTOP_OVERWRITABLE_LIMITED_CONTAINER_HPP_INCLUDED

#include <vector>
#include <cstddef>

template<typename T>
class OverwritableLimitedContainer {
public:
    explicit OverwritableLimitedContainer (const size_t pLimit)
        : mLimit(pLimit) {
        mVector.reserve(pLimit);
    }
    void push_back (const T& pValue) {
        if (mVector.size() < mLimit) {
            mVector.push_back(pValue);
        } else {
            if (begin + 1 <= mLimit) {
                mVector[begin] = pValue;
                begin += 1;
            } else {
                mVector[mLimit - 1] = pValue;
                begin = 0;
            }
        }
    }
    size_t size () const {
        return std::min(mVector.size(), mLimit);
    }
    T& operator[] (const size_t pNum) {
        size_t num = begin + pNum;
        if (num >= mLimit) {
            num -= mLimit;
        }
        return mVector[num];
    }

    const T& operator[] (const size_t pNum) const {
        size_t num = begin + pNum;
        if (num >= mLimit) {
            num -= mLimit;
        }
        return mVector[num];
    }

private:
    std::vector<T> mVector;
    const size_t mLimit;

    size_t begin = 0;
};


#endif // LFSTOP_OVERWRITABLE_LIMITED_CONTAINER_HPP_INCLUDED
