// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_USER_SETTINGS_HPP_INCLUDED
#define LFSTOP_USER_SETTINGS_HPP_INCLUDED


#include <vector>


#include <QBitArray>


#include "Databases/UsersDbStorage.hpp"
#include "Translation.hpp"


class UserSettings {
public:
    UserSettings (Translation::TranslationBinder * const pTrBind)
        : onTrackSelf (true, true, true, true),
          onTrackOthers (true, false, false, false),
          offTrack (true, true, true, true),
          mTr (pTrBind) {
    }

    void load (const UsersDbStorage::UserSettings &pSettings);

    UsersDbStorage::UserSettings getUserSettings () const;

    struct LaptimeUpdates {
        LaptimeUpdates (bool pPb, bool pTbs, bool pTb, bool pAvg)
            : pb (pPb),
              tbs (pTbs),
              tb (pTb),
              avg (pAvg) {

        }

        enum class Types : unsigned char { PB, TBS, TB, AVG };

        const bool &operator [] (const Types pType) const {
            switch (pType) {
            case Types::PB:
                return pb;
            case Types::TBS:
                return tbs;
            case Types::TB:
                return tb;
            case Types::AVG:
                return avg;
            }
            return pb;
        }

        static QByteArray typeToStr (const Types pVal) {
            switch (pVal) {
            case UserSettings::LaptimeUpdates::Types::PB:
                return "PB";
            case UserSettings::LaptimeUpdates::Types::TBS:
                return "TBS";
            case UserSettings::LaptimeUpdates::Types::TB:
                return "TB";
            case UserSettings::LaptimeUpdates::Types::AVG:
                return "AVG@3";
            }
            return "UWOT";
        }

        static const std::vector<Types> used_types;

        bool pb;
        bool tbs;
        bool tb;
        bool avg;
    };

    LaptimeUpdates onTrackSelf;
    LaptimeUpdates onTrackOthers;
    LaptimeUpdates offTrack;
    bool hlvcReasons = true;
    bool showSplits = false; // splits or sections
    bool queryClean = true; // not saved, coz fuck that shit


    QByteArray getLanguage () const;
    void setLanguage (const QByteArray &pLang);

private:
    QByteArray language = "?";
    Translation::TranslationBinder * const mTr;
};

#endif // LFSTOP_USER_SETTINGS_HPP_INCLUDED
