// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "UserSettingsInsimWindow.hpp"


using Translation::StringIds;


void UserSettingsInsimWindow::showWindow () {
    const size_t num = 1 + mSettingsNum + mTranslations->getLanguages().size();
    mCurrentButtonId = static_cast<unsigned char> (num);
    mButton.setUcid(mUcid);
    mButton.setAlignment(InsimButton::Alignment::center);

    const size_t num_langs = mTranslations->getLanguages().size();
    unsigned char num_lang_rows = 6;
    if (num_langs < 255) {
        const unsigned char nlangs = static_cast<unsigned char>(num_langs);
        num_lang_rows = nlangs / 8 + ((nlangs % 8) != 0 ? 1 : 0);
    }

    mNumRowsOnPage = 6 + num_lang_rows;

    mWindowHeight =
            mRowHeight * mNumRowsOnPage
            + mHeaderHeight
            + mNavButtonHeight
            + 2;
    mWindowCoordTop = (200 - mWindowHeight) / 2 + 5;

    sendBackgroundButton();
    sendSettingsButtons();

    sendOkButton();
}

void UserSettingsInsimWindow::clearWindow () {
    InsimButtonFunction bc (BFN_DEL_BTN, mUcid, 0);
    for (int i = 1; i <= mCurrentButtonId; ++i) {
        bc.setClickId(i);
        mSock->write(bc);
    }
}

void UserSettingsInsimWindow::buttonClick (const IS_BTC &pButtonClick) {
    if (pButtonClick.ClickID < mCurrentSettingsButtonId) {
        bool *set = nullptr;
        switch (pButtonClick.ClickID) {
        case 1:
            set = &(mUserSettings->onTrackSelf.pb);
            break;
        case 2:
            set = &(mUserSettings->onTrackSelf.tbs);
            break;
        case 3:
            set = &(mUserSettings->onTrackSelf.tb);
            break;
        case 4:
            set = &(mUserSettings->onTrackSelf.avg);
            break;
        case 5:
            set = &(mUserSettings->onTrackOthers.pb);
            break;
        case 6:
            set = &(mUserSettings->onTrackOthers.tbs);
            break;
        case 7:
            set = &(mUserSettings->onTrackOthers.tb);
            break;
        case 8:
            set = &(mUserSettings->onTrackOthers.avg);
            break;
        case 9:
            set = &(mUserSettings->offTrack.pb);
            break;
        case 10:
            set = &(mUserSettings->offTrack.tbs);
            break;
        case 11:
            set = &(mUserSettings->offTrack.tb);
            break;
        case 12:
            set = &(mUserSettings->offTrack.avg);
            break;
        case 13:
            set = &(mUserSettings->hlvcReasons);
            break;            
        }
        if (set == nullptr) {
            return;
        }

        *set = !(*set);

        mButton.setText((*set)
                        ? mTr->get(StringIds::ON)
                        : mTr->get(StringIds::OFF));
        mButton.setClickId(pButtonClick.ClickID);
        mButton.setClick(true);
        mButton.setDark(true);
        mButton.setCoords(0, 0, 0, 0);
        mSock->write(mButton);
    } else if (pButtonClick.ClickID < mCurrentLanguageSettingsButtonId) {
        mUserSettings->setLanguage(
            mTranslations->getLanguages().at((unsigned) pButtonClick.ClickID - mSettingsNum - 1));
        showWindow();
    }
    else {
        clearWindow ();
    }
}

void UserSettingsInsimWindow::sendOkButton () {
    mButton.setText(mTr->get(StringIds::OK));
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(true);
    mButton.setDark(true);
    mButton.setCoords(51 + mButtonWidth * 5,
                      mWindowCoordTop + mHeaderHeight + mRowHeight * mNumRowsOnPage,
                      mButtonWidth,
                      mNavButtonHeight);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendOnTrackHeader () {
    mButton.setText(mTr->get(StringIds::ON_TRACK));
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(false);
    mButton.setDark(true);
    mButton.setCoords(51,
                      mWindowCoordTop + mHeaderHeight + mRowHeight,
                      mHeaderButtonWidth,
                      mRowHeight * 2);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendOnTrackSelfHeader () {
    mButton.setText(mTr->get(StringIds::SELF));
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(false);
    mButton.setDark(true);
    mButton.setCoords(51 + mHeaderButtonWidth,
                      mWindowCoordTop + mHeaderHeight + mRowHeight,
                      mHeaderButtonWidth,
                      mRowHeight);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendOnTrackOthersHeader () {
    mButton.setText(mTr->get(StringIds::OTHERS));
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(false);
    mButton.setDark(true);
    mButton.setCoords(51 + mHeaderButtonWidth,
                      mWindowCoordTop + mHeaderHeight + mRowHeight * 2,
                      mHeaderButtonWidth,
                      mRowHeight);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendOffTrackHeader () {
    mButton.setText(mTr->get(StringIds::OFF_TRACK));
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(false);
    mButton.setDark(true);
    mButton.setCoords(51,
                      mWindowCoordTop + mHeaderHeight + mRowHeight * 3,
                      mHeaderButtonWidth * 2,
                      mRowHeight);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendUpdateTypesHeaders () {
    QList<QByteArray> l { "pb", "tbs", "tb", "avg" };
    mButton.setDark(false);
    mButton.setClick(false);
    for (int i = 0; i < 4; ++i) {
        mButton.setText(l.value(i));
        mButton.setClickId(mCurrentButtonId++);
        mButton.setCoords(51 + mHeaderButtonWidth * 2 + mButtonWidth * i,
                          mWindowCoordTop + mHeaderHeight,
                          mButtonWidth,
                          mRowHeight);

        mSock->write(mButton);
    }
}

void UserSettingsInsimWindow::sendUpdateTypesOptions (const int pNum,
                                                      const UserSettings::LaptimeUpdates &pUpd) {
    int i = 0;
    for (auto &upd_type : UserSettings::LaptimeUpdates::used_types) {
        mButton.setText(pUpd[upd_type]
                        ? mTr->get(StringIds::ON)
                        : mTr->get(StringIds::OFF));

        mButton.setClickId(mCurrentSettingsButtonId++);
        mButton.setClick(true);
        mButton.setDark(true);
        mButton.setCoords(51 + mHeaderButtonWidth * 2 + mButtonWidth * i,
                          mWindowCoordTop + mHeaderHeight
                          + mRowHeight * pNum + mRowHeight,
                          mButtonWidth,
                          mRowHeight);
        mSock->write(mButton);

        ++i;
    }
}

void UserSettingsInsimWindow::sendLaptimeUpdatesSettings () {
    sendOnTrackHeader();
    sendOnTrackSelfHeader();
    sendOnTrackOthersHeader();
    sendOffTrackHeader();
    sendUpdateTypesHeaders();

    sendUpdateTypesOptions(0, mUserSettings->onTrackSelf);
    sendUpdateTypesOptions(1, mUserSettings->onTrackOthers);
    sendUpdateTypesOptions(2, mUserSettings->offTrack);
}

void UserSettingsInsimWindow::sendHlvcPrintReasonsButton () {
    mButton.setText(mUserSettings->hlvcReasons
                    ? mTr->get(StringIds::ON)
                    : mTr->get(StringIds::OFF));
    mButton.setClickId(mCurrentSettingsButtonId++);
    mButton.setClick(true);
    mButton.setDark(true);
    mButton.setCoords(51 + mHeaderButtonWidth * 2 + mButtonWidth * 3,
                      mWindowCoordTop + mHeaderHeight + mRowHeight * 4,
                      mButtonWidth,
                      mRowHeight);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendHlvcPrintReasonsHeaderButtons () {
    mButton.setText(mTr->get(StringIds::HLVC_CHECK_INFO));
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(false);
    mButton.setDark(true);
    mButton.setCoords(51,
                      mWindowCoordTop + mHeaderHeight + mRowHeight * 4,
                      mHeaderButtonWidth * 2 + mButtonWidth * 3,
                      mRowHeight);
    mSock->write(mButton);
}

void UserSettingsInsimWindow::sendLanguageButtons () {
    int i = 0; // horisontal offset multiplier
    int j = 0; // vertical offset multiplier
    for (const QByteArray& lang : mTranslations->getLanguages()) {
        QByteArray lang_name =
            mTranslations->getExact(StringIds::LANGUAGE_NATIVE_NAME, lang);
        if (lang_name.isEmpty()) {
            // fall back to language built in name
            lang_name = Translation::getLanguageName(lang);
        }
        if (lang_name.isEmpty()) {
            // fall back to language code
            lang_name = lang;
        }

        mButton.setText(lang_name);

        mButton.setClickId(mCurrentLanguageSettingsButtonId++);
        mButton.setClick(true);
        mButton.setDark(true);
        mButton.setCoords(51 + mLanguageButtonWidth * i,
                          mWindowCoordTop + mHeaderHeight + mRowHeight * 6
                          + j * mRowHeight,
                          mLanguageButtonWidth,
                          mRowHeight - 1);
        mSock->write(mButton);
        ++i;
        if (i > 7) {
            i = 0;
            ++j;
        }
    }
}

void UserSettingsInsimWindow::sendSettingsButtons () {
    mCurrentSettingsButtonId = 1;
    mCurrentLanguageSettingsButtonId = 14;
    sendLaptimeUpdatesSettings();
    sendHlvcPrintReasonsButton();
    sendHlvcPrintReasonsHeaderButtons();
    sendLanguageButtons();
}

void UserSettingsInsimWindow::sendBackgroundButton () {
    mButton.setText("");
    mButton.setCoords(50, mWindowCoordTop, 100, mWindowHeight);
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(false);
    mButton.setDark(true);

    mSock->write(mButton);
}
