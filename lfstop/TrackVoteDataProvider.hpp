// Copyright 2015, 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_TRACK_VOTE_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_TRACK_VOTE_DATA_PROVIDER_HPP_INCLUDED


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "TrackVoteDispatcher.hpp"


class TrackVoteDataProvider final : public INonSwitchableInsimWindowDataProvider {
public:
    void setVoteDispatcher (TrackVoteDispatcher *pDispatcher) {
        mDispatcher = pDispatcher;
    }

    void startVote () {
        mVoteCasted = false;
        mDispatcher->totalVotesCast = 0;
    }

    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder &pTr) const override {
        (void) pTr;
        return mDispatcher->currentPrintableFields[pN];
    }

    void prepareData () override {
    }

    size_t getNumRows () const override {
        return mDispatcher->votableElementIds.size();
    }

    size_t getNumColumns () const override {
        switch(mDispatcher->currentMode) {
        case TrackVoteDispatcher::Mode::SITE:
            return 4;
        case TrackVoteDispatcher::Mode::TRACK:
            return 5;
        }
        return 4;
    }

    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        switch(mDispatcher->currentMode) {
        case TrackVoteDispatcher::Mode::SITE:
            return { { InsimButton::left, 10 },
                     { InsimButton::left, 40 },
                     { InsimButton::left, 40 },
                     { InsimButton::right, 10 },
            };
        case TrackVoteDispatcher::Mode::TRACK:
            return { { InsimButton::left, 10 },
                     { InsimButton::left, 40 },
                     { InsimButton::left, 15 },
                     { InsimButton::left, 25 },
                     { InsimButton::right, 10 },
            };
        }
        return {};
    }

    void rowClicked (const size_t pRowNum,
                     const unsigned short pMouseButtonClickFlags) override {
        if (pMouseButtonClickFlags == ISB_LMB && (!mVoteCasted)) {
            ++(mDispatcher->currentVotes[static_cast<size_t>(pRowNum)]);
            if (std::find(mDispatcher->updatedVotes.begin(),
                          mDispatcher->updatedVotes.end(),
                          pRowNum) == std::end(mDispatcher->updatedVotes)) {
                mDispatcher->updatedVotes.push_back(pRowNum);
                ++mDispatcher->totalVotesCast;
            }
            mVoteCasted = true;
        }
    }

    bool isInteractive () override {
        return true;
    }

    const QByteArray getRowColumn (const size_t pRow,
                                   const size_t pColumn) const override {
        assert(pColumn == mDispatcher->currentPrintableFields[0].size() - 1);
        return QByteArray::number((int)mDispatcher
                                      ->currentVotes[static_cast<size_t>(pRow)]);
    }



private:
    TrackVoteDispatcher *mDispatcher = nullptr;
    bool mVoteCasted = false;
};


#endif // LFSTOP_TRACK_VOTE_DATA_PROVIDER_HPP_INCLUDED
